import { Directive, Input, ViewContainerRef, ComponentFactoryResolver, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { FormInputComponent } from '../../components/form-input/form-input'
import { FormTextareaComponent } from '../../components/form-textarea/form-textarea'
import { FormSwitchComponent } from '../../components/form-switch/form-switch'
import { FormCheckboxComponent } from '../../components/form-checkbox/form-checkbox'
import { FormGpsComponent } from '../../components/form-gps/form-gps'
import { FormVideoComponent } from '../../components/form-video/form-video'
import { FormImageComponent } from '../../components/form-image/form-image'
import { FormSliderComponent } from '../../components/form-slider/form-slider'
import { FormDataTimeComponent } from '../../components/form-data-time/form-data-time'
import { FormCategoryComponent } from '../../components/form-category/form-category';

const componentMapper = {
  text: FormInputComponent,
  textarea: FormTextareaComponent,
  switch: FormSwitchComponent,
  checkbox: FormCheckboxComponent,
  gps: FormGpsComponent,
  video: FormVideoComponent,
  image: FormImageComponent,
  slider: FormSliderComponent,
  datetime: FormDataTimeComponent,
  category: FormCategoryComponent
}

@Directive({
  selector: '[dynamicField]'
})

export class DynamicFieldDirective implements OnInit {

  @Input() config
  @Input() group: FormGroup
  @Input() rootConfig
  componentRef: any

  constructor(
    // ComponentFactoryResolver se usará para resolver el componente en tiempo de ejecución.
    private resolver: ComponentFactoryResolver,
    // ViewContainerRef para obtener acceso al contenedor de vista del elemento que alojará el componente agregado dinámicamente.
    private container: ViewContainerRef
  ) { }

  ngOnInit() {
    if (this.config.type !== 'category') {
      // método resolveComponentFactory que se puede usar para crear un componente en tiempo de ejecución.
      const factory = this.resolver.resolveComponentFactory<any>(componentMapper[this.config.value])
      // para acceder al componente en otros métodos si es necesario.
      this.componentRef = this.container.createComponent(factory)
      // pasar la configuración y el grupo a nuestro componente creado dinámicamente
      this.componentRef.instance.config = this.config
      this.componentRef.instance.group = this.group
    } else {
      const factory = this.resolver.resolveComponentFactory<any>(componentMapper[this.config.type])
      this.componentRef = this.container.createComponent(factory)
      this.componentRef.instance.config = this.config
      this.componentRef.instance.group = this.group
      this.componentRef.instance.rootConfig = this.rootConfig
    }
  }

}
