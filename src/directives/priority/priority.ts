import { Directive, Input, OnInit, ElementRef } from '@angular/core'

@Directive({
  selector: '[priority]'
})

export class PriorityDirective implements OnInit{

  @Input('priority') priority: string = '';

  constructor(
    private elRef: ElementRef,
  ) { }

  ngOnInit() {
    if (this.priority === 'high-bg') {
      this.elRef.nativeElement.style.background = '#ec595f'
    }
    else if (this.priority === 'medium-bg') {
      this.elRef.nativeElement.style.background = '#f9a750'
    }
    else if (this.priority === 'low-bg') {
      this.elRef.nativeElement.style.background = '#4a82e4'
    }
    else if (this.priority === 'critical-bg') {
      this.elRef.nativeElement.style.background = '#d20d0d'
    }
    if (this.priority === 'hight-cl') {
      this.elRef.nativeElement.style.color = '#ec595f'
    }
    else if (this.priority === 'medium-cl') {
      this.elRef.nativeElement.style.color = '#f9a750'
    }
    else if (this.priority === 'low-cl') {
      this.elRef.nativeElement.style.color = '#4a82e4'
    }
    else if (this.priority === 'critical-cl') {
      this.elRef.nativeElement.style.color = '#d20d0d'
    }
  }

}
