import { Pipe, PipeTransform } from '@angular/core';
import { _ } from 'underscore'

@Pipe({
  name: 'state',
})

export class StatePipe implements PipeTransform {
  transform(value: any, ...args) {
    if (_.isString(value)) {
      if (value.toLowerCase() === 'in progress') return 'En progreso'
      else if (value.toLowerCase() === 'completed') return 'Completada'
      else if (value.toLowerCase() === 'low') return 'Baja'
      else if (value.toLowerCase() === 'medium') return 'Media'
      else if (value.toLowerCase() === 'high') return 'Alta'
      else if (value.toLowerCase() === 'critical') return 'Critica'
      else if (value.toLowerCase() === 'not started') return 'No iniciada'
      else if (value.toLowerCase() === 'no started') return 'No iniciada'
      else if (value.toLowerCase() === 'expired') return 'Expirada'
      else return value
    }
    else if (_.isNumber(value)) {
      if (value.toString() === '1') return 'No iniciada'
      else if (value.toString() === '2') return 'En progreso'
      else if (value.toString() === '3') return 'Completada'
      else if (value.toString() === '4') return 'Expirada'
    }
    else if(value === null) return 'Sin iniciar'
    else return value
  }
}
