import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'filterList'
})

export class FilterListPipe implements PipeTransform {
  transform(items: Array<any>, searchText: string, type: string, ...args) {
    if(!items) return []
    if(!searchText) return items
    searchText = searchText.toLowerCase()
    if (type === 'inspection') {
      return items.filter(it => {
        let result = `${it.created_at}${it.expired_at}${it.scheduled ? it.scheduled.title : ''}${it.state ? it.state.title : ''}${it.template.title}}`
            result.replace('in progress', 'en progreso')
            result.replace('completed', 'completada')
            result.replace('not started', 'no iniciada')
            result.replace('expired', 'expirada')
        return result.toLowerCase().includes(searchText)
      })
    }
    else if (type === 'template') {
      return items.filter(it => {
        let result = `${it.created_at}${it.description}${it.title}}`
        return result.toLowerCase().includes(searchText)
      })
    }
    else if (type === 'action') {
      return items.filter(it => {
        let result = `${it.created_at}${it.date_time_end}${it.description}${it.priority}`
            result.replace('low', 'bajo')
            result.replace('medium', 'medio')
            result.replace('high', 'alto')
            result.replace('critical', 'critico')
        return result.toLowerCase().includes(searchText)
      })
    }
    else if (type === 'users') {
      return items.filter(it => {
        let result = `${it.name || ''}${it.username || ''}${it.email || ''}`
        return result.toLowerCase().includes(searchText.toLowerCase())
      })
    }
  }
}
