import { Component, OnInit } from '@angular/core'
import { ViewController, NavController, App, Platform } from 'ionic-angular'
import { File } from '@ionic-native/file'
import { FileOpener } from '@ionic-native/file-opener'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFonts from 'pdfmake/build/vfs_fonts'
import { _ } from 'underscore'
import base64Img from 'base64-img'

pdfMake.vfs = pdfFonts.pdfMake.vfs

@Component({
  selector: 'inspection-alert',
  templateUrl: 'inspection-alert.html'
})

export class InspectionAlertComponent implements OnInit {

  pdfObject: any
  responsesInspection: Array<any>
  inspection: any

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public appCtrl: App,
    public file: File,
    public fileOpener: FileOpener,
    public platform: Platform
  ) { }

  ngOnInit() {
    this.responsesInspection = this.viewCtrl.data.pdfResponses
    this.inspection = this.viewCtrl.data.inspection
  }

  ionViewWillEnter() {
  }

  confirm() {
    this.viewCtrl.dismiss(true)
  }

  generatePDF() {
    let content: any = [
      {
        text: 'Check on Site\n\n',
        style: 'header',
        bold: true,
        fontSize: 15
      },
      {
        text: [
          `${this.inspection.template.title}\n`,
          `${this.inspection.template.description}\n\n`
        ],
        bold: false,
        fontSize: 12
      },
      {
        table: {
          fontSize: 12,
          widths: ['*', '*', '*'],
          body: [
            ['Pregunta', 'Respuesta', 'Observaciones']
          ]
        }
      }
    ]
    this.responsesInspection.forEach(item => {
      let observations = this.getObservations(parseInt(item.code))
      let images = this.getImages(parseInt(item.code))
      let text = []
      let bodyPDF = content[2].table.body
      text.push(`${item.question} : \n`)
      text.push(`${item.response.toString()} \n\n`)
      let textObservations = this.convertObservationsToString(observations)
      text.push(textObservations)
      if (bodyPDF) {
        bodyPDF.push(text)
        let promises = []
        images.forEach(img => {
          promises.push(this.converToBase64(img.original_file))
        })
        if (promises.length > 0) {
          Promise.all(promises).then((res: Array<string>) => {

              res.forEach(img64 => {
                bodyPDF.push([{
                  image: img64,
                  width: 200,
                  height: 200,
                  colSpan: 3
                }])
                let pdfDefinition = { content }
                this.pdfObject = pdfMake.createPdf(pdfDefinition)
              })
            })
            .catch(error => {
              debugger
            })
        } else {
          let pdfDefinition = { content }
          this.pdfObject = pdfMake.createPdf(pdfDefinition)
        }
      }
    })
  }

  private converToBase64(url) {
    return new Promise((resolve, reject) => {
      base64Img.requestBase64(url, (err, res, body) => {
        if (body)
          resolve(body)
        else
          reject(err)
      })
    })
  }

  private convertObservationsToString(observations: any[]) {
    let textObservations = ''
    observations.forEach(obs => {
      textObservations += `${obs.description} \n`
    })
    return textObservations
  }

  openPDF() {
    if (this.platform.is('cordova')) {
      this.pdfObject.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' })
        this.file.writeFile(this.file.dataDirectory, 'inspeccion.pdf', blob, { replace: true }).then(fileEntry => {
          this.fileOpener.open(this.file.dataDirectory + 'inspeccion.pdf', 'application/pdf')
        })
      })
      return true
    }
    this.pdfObject.download()
  }

  getObservations(itemCode: number): Array<any> {
    if (this.inspection)
      return this.inspection.observations.filter(item => item.item_code === itemCode)
    return []
  }

  getImages(itemCode: number): Array<any> {
    if (this.inspection)
      return this.inspection.images.filter(item => item.item_code === itemCode)
    return []
  }
}
