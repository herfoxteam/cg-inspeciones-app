import { Component, OnInit } from '@angular/core'
import { ViewController, NavParams, ModalController, LoadingController } from 'ionic-angular'
import { CreateAction } from '../action-modal-actions/create-action'
import { InspectionsProvider } from '../../providers/inspections/inspections';
import { _ } from 'underscore'

@Component({
  selector: 'modal-list-acions',
  templateUrl: 'modal-list-acions.html'
})

export class ModalListAcionsComponent implements OnInit {

  actions: Array<any> = []
  code: number
  loaderGetInspection: any

  constructor(
    public viewCtrl: ViewController,
    private params: NavParams,
    public modalCtrl: ModalController,
    private inspectionsProvider: InspectionsProvider,
    public loadingCtrl: LoadingController
  ) { }

  ionViewWillEnter() {
    this.getInspectionById()
  }

  ngOnInit() {
    this.code = this.params.data.code
  }

  getInspectionById() {
    this.presentLoading('loaderGetInspection', 'cargando')

    let id = JSON.parse(localStorage.getItem('inspectionId'))
    this.inspectionsProvider.getInspectionByIdOffline(id)
      .then(val => {
        let actualInspection = val
        let actions = actualInspection.actions
        this.actions = this.findActionsById(this.code, actions)
        this.loaderGetInspection.dismiss()
      }, error => {
        this.loaderGetInspection.dismiss()
      })
  }

  dismiss() {
    this.viewCtrl.dismiss(this.actions)
  }

  findActionsById(id: number, list: Array<any>): Array<any> {
    let result: any[] = []
    list.forEach(item => {
      if (item.item) {
        if (item.item.code === id.toString()) {
          result.push(item)
        }
      } else if (item.item_code === id) {
        result.push(item)
      }
    })
    return result
  }

  createAction() {
    const modal = this.modalCtrl.create(CreateAction, { code: this.code })
    modal.present()
    modal.onDidDismiss(actions => {
      if (actions) {
        this.actions = this.findActionsById(this.code, actions)
      }
    })
  }

  presentLoading(loader: string, text?: string) {
    this[loader] = this.loadingCtrl.create({
      content: `${text}...`
    })
    this[loader].present()
  }

}
