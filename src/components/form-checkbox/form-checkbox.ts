import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ModalController, ViewController } from 'ionic-angular'
import { MultipleSelectionComponent, OptionSelect } from '../multiple-selection/multiple-selection'
import { _ } from 'underscore'

@Component({
  selector: 'form-checkbox',
  template: `
    <div [formGroup]="group" class="inspections__box" id="{{config.code}}" [class.invalid]="!valid">
    <p class="inspections__label">{{ config.storage.label }}</p>
    <button ion-button class="btn__init" (click)="openModal()">Seleccionar respuestas</button>
    <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
    `
  })

  // <div [formGroup]="group" class="inspections__box" id="{{config.code}}" [class.invalid]="!group.controls[config.code].valid">
export class FormCheckboxComponent implements OnInit {

  config
  group: FormGroup
  typeSelection: string
  response: Array<any>
  valid: boolean

  constructor(
    public modalCtrl: ModalController,
    public viewCtrl: ViewController
  ) { }

  ngOnInit() {
    this.group.valueChanges.subscribe(val => {
      if (this.group.controls[this.config.code].valid === true)
        this.valid = true
      else
        this.valid = false
    })
  }

  openModal() {
    let viewModal = this.modalCtrl.create(MultipleSelectionComponent, { config: this.config, group: this.group }, { cssClass: 'modal__multiselect' })
    viewModal.present()
    viewModal.onDidDismiss((res: Array<OptionSelect>) => {
      this.response = []
      res.forEach(item => {
        if (item.checked) {
          this.response.push({code: item.code, checked: item.checked})
        }
      })
      this.group.controls[this.config.code].setValue(this.response)
    })
  }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = event.images
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }
}
