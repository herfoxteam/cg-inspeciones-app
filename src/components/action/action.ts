import { Component, Input, OnInit } from '@angular/core'
import { Action } from '../../pages/actions/action.model'
import { NavController } from 'ionic-angular';
import { ActionDetailPage } from '../../pages/action-detail/action-detail'

@Component({
  selector: 'inspection-action',
  templateUrl: 'action.html'
})

export class ActionComponent implements OnInit {

  @Input() action: Action

  constructor(
    public navCtrl: NavController
  ) { }

  ngOnInit() { }

  goToActionDetail(id: number) {
    this.navCtrl.push(ActionDetailPage, this.action)
  }

}
