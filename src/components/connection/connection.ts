import { Component, Output, EventEmitter } from '@angular/core';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'connection',
  templateUrl: 'connection.html'
})

export class ConnectionComponent {

  @Output() connection: EventEmitter<boolean> = new EventEmitter<boolean>();
  isConnect: boolean

  constructor(
    private network: Network
  ) {
    // debugger
    this.network.onchange().subscribe(res => {
      debugger
    })
    this.network.onDisconnect().subscribe(() => {
      debugger
      this.isConnect = false
      this.connection.emit(this.isConnect)
      console.log('network was disconnected :-(')
    })

    this.network.onConnect().subscribe(() => {
      debugger
      this.isConnect = true
      this.connection.emit(this.isConnect)
      console.log('network connected!')
    })
  }

}
