import { Component } from '@angular/core'
import { ViewController } from 'ionic-angular'

@Component({
  selector: 'modal-terms',
  templateUrl: 'modal-terms.html'
})

export class ModalTermsComponent {

  constructor(public viewCtrl: ViewController) { }

  dismiss() {
    this.viewCtrl.dismiss()
  }

}
