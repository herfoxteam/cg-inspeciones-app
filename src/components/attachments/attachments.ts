import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core'
import { AlertController, ModalController } from 'ionic-angular'
import { ActionModalCommentsComponent } from '../action-modal-comments/action-modal-comments'
import { ModalListAcionsComponent } from '../modal-list-acions/modal-list-acions'
import { ActionModalImagesComponent } from '../action-modal-images/action-modal-images'
import { _ } from 'underscore'

@Component({
  selector: 'attachments',
  templateUrl: 'attachments.html'
})

export class AttachmentsComponent implements OnInit {

  objectActions: any = {
    observations: [],
    images: [],
    actions: []
  }
  @Input() code: number
  @Input() actions: any
  @Output() changeActions: EventEmitter<any> = new EventEmitter()
  lengthAction: number = 0
  lengthObservations: number = 0
  lengthImages: number = 0
  modalData: any

  constructor(
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.modalData = this.actions.value.actions
    this.calculeLengthAttachments()
  }

  calculeLengthAttachments() {
    if(this.modalData.actions)
      this.lengthAction = this.lengthById(this.code, this.modalData.actions).length
    if (this.modalData.observations)
      this.lengthObservations = this.lengthById(this.code, this.modalData.observations).length
    if (this.modalData.images)
      this.lengthImages = this.lengthById(this.code, this.modalData.images).length
  }

  lengthById(id: number, list: Array<any>): Array<any> {
    let result: any[] = []
    list.forEach(item => {
      if (item.item) {
        if (item.item.code === id.toString()) {
          result.push(item)
        }
      } else if (item.item_code === id) {
        result.push(item)
      }
    })
    return result
  }

  openModalActions() {
    const modal = this.modalCtrl.create(ModalListAcionsComponent, { code: this.code })
    modal.present()
    modal.onDidDismiss(data => {
      if (_.isArray(data)) {
        // this.objectActions.actions = _.union(this.modalData.actions, data)
        this.objectActions.actions = data
        this.modalData.actions = this.objectActions.actions
        this.calculeLengthAttachments()
        this.changeActions.emit(this.objectActions)
      }
    })
  }

  openModalComments() {
    const modal = this.modalCtrl.create(ActionModalCommentsComponent, { code: this.code, observations: this.modalData.observations })
    modal.present()
    modal.onDidDismiss(data => {
      if (_.isArray(data)) {
        this.objectActions.observations = _.union(this.modalData.observations, data)
        this.modalData.observations = this.objectActions.observations
        this.calculeLengthAttachments()
        this.changeActions.emit(this.objectActions)
      }
    })
  }

  openModalImages() {
    const modal = this.modalCtrl.create(ActionModalImagesComponent, { code: this.code, images: this.modalData.images })
    modal.present()
    modal.onDidDismiss(data => {
      if (_.isArray(data)) {
        this.objectActions.images = data
        this.modalData.images = this.objectActions.images
        this.calculeLengthAttachments()
        this.changeActions.emit(this.objectActions)
      }
    })
  }

}
