import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture'
import { MediaProvider } from '../../providers/media/media'
import { ActionSheetController, Platform, AlertController } from 'ionic-angular'
import { CameraOptions, Camera } from '@ionic-native/camera'
import { environment } from '../../environments/environment'
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media'
import { File } from '@ionic-native/file'
import { _ } from 'underscore'
import { VideoEditor, CreateThumbnailOptions } from '@ionic-native/video-editor';

@Component({
  selector: 'form-video',
  template: `
    <div class="inspections__box" [formGroup]="group" id="{{config.code}}">
      <p class="inspections__label">{{ config.storage.label }}</p>
      <button color="primary" ion-button (click)="openMenu()">
        <ion-icon name="videocam"></ion-icon>
        Añadir Video
      </button>
      <div class="loading" *ngIf="loadingMedia">
        <ion-spinner></ion-spinner>
      </div>
      <ul class="inspections__gallery">
        <li *ngFor="let media of listMedia" (click)="playVideo(media)" class="video">
          <div class="item" [ngStyle]="{'background-image': 'url('+media.thumbnail+')'}"></div>
          <div class="play"></div>
          <ion-icon name="close-circle" class="exit" (click)="removeVideo(media)"></ion-icon>
        </li>
      </ul>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
  `
})

export class FormVideoComponent implements OnInit {

  config
  group: FormGroup
  listMedia: any[] = []
  urlBase = environment.urlBase
  loadingMedia: boolean = false
  lastVideo: string = null
  win: any = window

  constructor(
    private mediaCapture: MediaCapture,
    private mediaProvider: MediaProvider,
    public actionsheetCtrl: ActionSheetController,
    public platform: Platform,
    private camera: Camera,
    public alertCtrl: AlertController,
    public file: File,
    private streamingMedia: StreamingMedia,
    private videoEditor: VideoEditor
  ) { }

  ngOnInit() {
    let responses = this.group.controls[this.config.code].value
    if (responses && responses !== '') {
      responses.forEach(item => {
        this.listMedia.push(item)
      })
    }
  }

  captureVideo() {
    let options: CaptureVideoOptions = { duration: 60, quality: 70 }
    this.mediaCapture.captureVideo(options)
      .then((data: MediaFile[]) => {
        if (this.platform.is('android')) {
          let correctPath = data[0].fullPath.substr(0, data[0].fullPath.lastIndexOf('/') + 1)
          let currentName = data[0].fullPath.substr(data[0].fullPath.lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileVideo(currentName))
        } else {
          var currentName = data[0].fullPath.substr(data[0].fullPath.lastIndexOf('/') + 1)
          var correctPath = data[0].fullPath.substr(0, data[0].fullPath.lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileVideo(currentName))
        }
      }, error => {

      })
  }

  uploadVideo() {
    let options: CameraOptions = {
      quality: 70,
      destinationType: 1,
      sourceType: 0,
      allowEdit: true,
      mediaType: 1
    }

    this.loadingMedia = true
    this.camera.getPicture(options)
      .then((urlVideo) => {
        if (this.platform.is('android')) {
          let correctPath = urlVideo.substr(0, urlVideo.lastIndexOf('/') + 1)
          let currentName = urlVideo.substr(urlVideo.lastIndexOf('/') + 1)
          this.copyFileToLocalDir('file://' + correctPath, currentName, this.createNewNameForFileVideo(currentName))
        } else {
          var currentName = urlVideo.substr(urlVideo.lastIndexOf('/') + 1)
          var correctPath = urlVideo.substr(0, urlVideo.lastIndexOf('/') + 1)
          this.copyFileToLocalDir('file://' + correctPath, currentName, this.createNewNameForFileVideo(currentName))
        }
      }, error => {
        const alert = this.alertCtrl.create(error)
        alert.present()
      })
  }

  requestVideo(urlVideo: string, thumbnail?: string) {
    thumbnail = this.win.Ionic.WebView.convertFileSrc(thumbnail)
    this.listMedia.push({
      route_video: urlVideo,
      thumbnail
    })
    debugger
    this.group.controls[this.config.code].setValue(this.listMedia)

    // this.mediaProvider.processingVideo(urlVideo)
    //   .subscribe(res => {
    //     debugger
    //     let video = JSON.parse(res.response)
    //     this.listMedia.push({
    //       route_video: video.data.route_video,
    //       thumbnail: video.data.thumbnail
    //     })
    //     this.group.controls[this.config.code].setValue(this.listMedia)
    //     this.loadingMedia = false
    //   }, error => {
    //     debugger
    //     this.loadingMedia = false
    //     const alert = this.alertCtrl.create(error.body)
    //     alert.present()
    //   })
  }

  openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Video',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Capturar video',
          icon: !this.platform.is('ios') ? 'share' : null,
          handler: () => this.captureVideo()
        },
        {
          text: 'Cargar video',
          icon: !this.platform.is('ios') ? 'arrow-dropright-circle' : null,
          handler: () => this.uploadVideo()
        },
        {
          text: 'Cancelar',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            this.loadingMedia = false
            console.log('Cancel clicked')
          }
        }
      ]
    })
    actionSheet.present()
  }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

  removeVideo(video) {
    let index = _.indexOf(this.listMedia, video)
    if (_.isNumber(index)) {
      this.listMedia.splice(index, 1)
      this.group.controls[this.config.code].setValue(this.listMedia)
    }
  }

  playVideo(video) {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => {
        debugger
      },
      shouldAutoClose: true,
      controls: true,
      orientation: 'portrait'
    }
    this.streamingMedia.playVideo(video.route_video, options);
  }

  createNewNameForFileVideo(url) {
    let ext = url.substr(url.lastIndexOf('.') + 1)
    let d = new Date(),
      n = d.getTime(),
      newFileName = `${n}.${ext}`
    return newFileName
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.loadingMedia = true
    this.file.copyFile(namePath, currentName, this.file.externalDataDirectory + 'videos', newFileName)
      .then(success => {
        this.lastVideo = newFileName
        let video = this.pathForVideo(this.lastVideo)
        var option: CreateThumbnailOptions = {
          fileUri: video,
          width: 200,
          height: 200,
          atTime: 1,
          outputFileName: newFileName.substr(0, newFileName.lastIndexOf('.') + 0),
          quality: 80
        }
        this.videoEditor.createThumbnail(option).then(result => {
          let thumbnail = 'file://' + result
          this.requestVideo(video, thumbnail)
          this.loadingMedia = false
        }).catch(e => {
          this.loadingMedia = false
          debugger
          // alert('fail video editor')
        });
      }, error => {
        this.loadingMedia = false
        debugger
      })
  }

  pathForVideo(video) {
    if (video === null) {
      return ''
    } else {
      return this.file.externalDataDirectory + 'videos/' + video
    }
  }

}
