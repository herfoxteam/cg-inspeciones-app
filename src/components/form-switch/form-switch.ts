import { Component } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { _ } from 'underscore'

@Component({
  selector: 'form-switch',
  template: `
    <ion-list class="inspections__box" [formGroup]="group" id="{{config.code}}">
      <ion-item>
        <ion-label class="inspections__label">{{ config.storage.label }}</ion-label>
        <ion-toggle [formControlName]="config.code"></ion-toggle>
      </ion-item>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </ion-list>
  `
})

export class FormSwitchComponent {

  config
  group: FormGroup

  constructor() { }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

}
