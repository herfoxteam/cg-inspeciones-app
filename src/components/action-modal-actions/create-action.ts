import { Component, OnDestroy } from '@angular/core'
import { ViewController, NavParams, AlertController, ModalController } from 'ionic-angular'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ActionsProvider } from '../../providers/actions/actions'
import { debounceTime } from 'rxjs/operators'
import { DatePipe } from '@angular/common'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection';
import { _ } from 'underscore'
import { Action } from '../../pages/actions/action.model';

@Component({
  selector: 'create-action',
  templateUrl: 'create-action.html'
})

export class CreateAction implements OnDestroy {

  code: any
  formAction: FormGroup
  users: Array<any>
  userSelected: any = null
  lengthActions: number
  currentAction: any
  modEdition: boolean = false
  subscription: any
  minDateAction = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
  searchText: string = ''
  selectedUser: boolean = true

  constructor(
    public viewCtrl: ViewController,
    private params: NavParams,
    private _formBuilder: FormBuilder,
    public actionsProvider: ActionsProvider,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private datePipe: DatePipe,
    private storage: Storage,
    private connectionProvider: ConnectionProvider
  ) {
    this.inicializeFormAction()
    this.getUsers()
  }

  ionViewWillEnter() {
    this.code = this.params.data.code

    if (this.params.data.action) {
      if (_.isNumber(this.params.data.action.id)) {
        this.modEdition = true
        this.currentAction = this.params.data.action
        this.userSelected = this.currentAction.assigned
        this.currentAction.priority = this.getPriotity(this.currentAction.priority)
        this.currentAction.date_time_end = this.datePipe.transform(this.currentAction.date_time_end, 'yyyy-MM-dd')
        this.formAction = this._formBuilder.group({
          item_code: [this.code || this.currentAction.item_code, Validators.required],
          description: [this.currentAction.description || '', Validators.required],
          date_time_end: [this.currentAction.date_time_end || '', Validators.required],
          priority: [this.currentAction.priority || 1, Validators.required],
          assigned: [this.userSelected.name || '', Validators.required],
          inspection_id: [parseInt(localStorage.getItem('inspectionId')), Validators.required]
        })
      }
    } else this.inicializeFormAction()

    this.detectChangesAutocomplete()
  }

  getUsers() {
    this.storage.get('users')
      .then((val) => {
        debugger
        this.users = JSON.parse(val) || []
      })
      .catch(error => {
        debugger
        this.users = []
      })
  }

  inicializeFormAction() {
    this.formAction = this._formBuilder.group({
      item_code: [this.code || '', Validators.required],
      description: ['', Validators.required],
      date_time_end: ['', Validators.required],
      priority: [1, Validators.required],
      assigned: ['', Validators.required],
      inspection_id: [parseInt(localStorage.getItem('inspectionId')), Validators.required]
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  detectChangesAutocomplete() {
    if (this.formAction) {
      this.subscription = this.formAction.controls.assigned.valueChanges.subscribe(val => {
        if (this.userSelected) {
          if (this.userSelected.name) {
            if (val !== this.userSelected.name) {
              this.userSelected = ''
              this.selectedUser = true
            }
          }
        }
        else if (val !== '' || val !== ' ')
          this.userSelected = ''
        // this.actionsProvider.getResults(val)
        //   .subscribe(res => {
        //     if (res)
        //       this.users = res
        //     else
        //       this.users = []
        //   }, error => {
        //     debugger
        //   })
      })
    }
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }

  selectResponsable(user: any) {
    this.subscription.unsubscribe()
    this.formAction.controls.assigned.setValue(user.name)
    this.detectChangesAutocomplete()
    this.userSelected = user
    this.selectedUser = false
  }

  saveChanges() {
    if (!this.formAction.value.priority)
      this.formAction.controls.priority.setValue(1)
    if (this.formAction.valid) {
      let { newAction, action }: { newAction: Action; action: any; } = this.buildActionObjectForRequest()

      if (this.modEdition)
        this.updateAction(newAction, action)
      else
        this.createAction(newAction, action)

    } else {
      const alert = this.alertCtrl.create({
        subTitle: 'Campos de acción invalidos',
        message: 'Verifique todos los campos por favor'
      })
      alert.present()
    }
  }

  buildActionObjectForRequest() {
    let action = this.formAction.value
    action.priority = this.assignPriotity(action.priority)
    let newAction: Action = {
      id: new Date().getTime(),
      description: action.description,
      date_time_end: action.date_time_end,
      assigned_user_id: this.userSelected.id,
      closed: false,
      priority: action.priority,
      inspection_id: action.inspection_id,
      item_code: action.item_code,
      created_at: new Date(),
      updated_at: new Date(),
      states_actions_id: 1,
      assigned: this.userSelected,
      state: {
        id: 1,
        title: 'not started',
        description: ''
      },
      sync: false
    }
    // let newAction: any = {
    //   assigned_user_id: this.userSelected.id,
    //   date_time_end: action.date_time_end,
    //   description: action.description,
    //   inspection_id: action.inspection_id,
    //   item_code: action.item_code,
    //   priority: action.priority
    // }
    return { newAction, action }
  }

  createAction(newAction: Action, action: any) {
    this.actionsProvider.createActionOffline(newAction)
      .then(res => {
        this.viewCtrl.dismiss(res)
      })
      .catch(error => {
        debugger
      })
    // this.actionsProvider.createAction(newAction)
    //   .subscribe(res => {
    //     action.assigned = this.userSelected
    //     action.id = res.data.id
    //     this.viewCtrl.dismiss(action)
    //   }, error => {
    //     debugger
    //   })
  }

  updateAction(newAction: Action, action: any) {
    newAction.code = this.currentAction.code || this.currentAction.item_code
    this.actionsProvider.updateActionOffline(this.currentAction.id, newAction)
      .then(val => {
        debugger
        action.assigned = this.userSelected
        this.viewCtrl.dismiss(action)
      })
      .catch(error => {
        debugger
      })
    // this.actionsProvider.updateAction(this.currentAction.id, newAction)
    //   .subscribe(res => {
    //     action.assigned = this.userSelected
    //     this.viewCtrl.dismiss(action)
    //   }, error => {
    //     debugger
    //   })
  }

  assignPriotity(id: number): string {
    let result
    if (id === 1)
      result = 'low'
    if (id === 2)
      result = 'medium'
    if (id === 3)
      result = 'high'
    if (id === 4)
      result = 'critical'
    return result
  }

  getPriotity(name: string): string {
    let result
    if (name === 'low')
      result = 1
    if (name === 'medium')
      result = 2
    if (name === 'high')
      result = 3
    if (name === 'critical')
      result = 4
    return result
  }

}
