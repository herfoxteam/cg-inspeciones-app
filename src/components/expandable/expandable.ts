import { Component, ViewChild, ElementRef, Input, Renderer } from '@angular/core'

@Component({
  selector: 'expandable',
  template: `
    <p class="title"  (click)="expandCollapse($event)">
      <span>{{title}}</span>
      <ion-icon name="arrow-dropdown" [class.active]="!expanded"></ion-icon>
    </p>
    <div #expandWrapper class='expand-wrapper' [class.collapsed]="!expanded">
      <ng-content></ng-content>
    </div>
  `
})

export class ExpandableComponent {

  @ViewChild('expandWrapper', { read: ElementRef }) expandWrapper
  @Input('expanded') expanded: boolean
  @Input('title') title: string

  constructor(public renderer: Renderer) { }

  ngAfterViewInit() {
    this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', '100%');
  }

  expandCollapse() {
    this.expanded = !this.expanded
  }

}
