import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ActionSheetController, Platform, AlertController } from 'ionic-angular'
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection'
import { File } from '@ionic-native/file'
import { CameraOptions, Camera } from '@ionic-native/camera'
import { FilePath } from '@ionic-native/file-path'
import { _ } from 'underscore'
import { environment } from '../../environments/environment'

@Component({
  selector: 'form-image',
  template: `
    <div class="inspections__box" [formGroup]="group" id="{{config.code}}">
      <p class="inspections__label">{{ config.storage.label }}</p>
      <button color="primary" ion-button (click)="openSheetPhoto()">
        <ion-icon name="camera"></ion-icon>
        Añadir Foto
      </button>
      <div class="loading" *ngIf="loadingPhotos">
        <ion-spinner></ion-spinner>
      </div>
      <ul class="inspections__gallery">
        <li *ngFor="let photo of listPhotos">
          <div class="item" imageViewer="{{photo.route_image_original}}" [ngStyle]="{'background-image': 'url('+photo.route_image_thumbnail+')'}"></div>
          <ion-icon name="close-circle" class="exit" (click)="removeImage(photo)"></ion-icon>
        </li>
      </ul>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
  `
})

export class FormImageComponent implements OnInit {

  config
  group: FormGroup
  listPhotos: Array<any> = []
  urlBase = environment.urlBase
  loadingPhotos: boolean = false
  lastImage: string = null
  win: any = window

  constructor(
    public actionsheetCtrl: ActionSheetController,
    public platform: Platform,
    private imagePicker: ImagePicker,
    public alertCtrl: AlertController,
    private storage: Storage,
    private connectionProvider: ConnectionProvider,
    public file: File,
    public filePath: FilePath,
    private camera: Camera
  ) { }

  ngOnInit() {
    let responses = this.group.controls[this.config.code].value
    if (responses && responses !== '') {
      responses.forEach(item => {
        this.listPhotos.push(item)
      })
    }
  }

  removeImage(photo) {
    let index = _.indexOf(this.listPhotos, photo)
    debugger
    if (_.isNumber(index)) {
      this.listPhotos.splice(index, 1)
      this.group.controls[this.config.code].setValue(this.listPhotos)
    }
  }

  captureImage() {
    let options: CameraOptions = {
      quality: 70,
      destinationType: 2,
      sourceType: 1,
      allowEdit: true,
      saveToPhotoAlbum: true,
      correctOrientation: true
    }
    this.camera.getPicture(options)
      .then(imagePath => {
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(imagePath)
            .then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1)
              let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1)
              this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
            })
        } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1)
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
        }
      }, error => {
        debugger
      })
  }

  uploadPhoto() {
    let options: ImagePickerOptions = {
      quality: 75,
      outputType: 0,
      width: 500
    }
    this.imagePicker.hasReadPermission()
      .then(res => {
        if (res)
          this.selectFromPickerImages(options)
        else {
          this.imagePicker.requestReadPermission()
            .then(res => {
              if (res === 'ok')
                this.selectFromPickerImages(options)
            })
        }
      })
      .catch(error => console.log(error))
  }

  selectFromPickerImages(options: ImagePickerOptions) {
    this.imagePicker.getPictures(options).then((results) => {
      for (let i = 0; i < results.length; i++) {
        if (this.platform.is('android')) {
          let correctPath = results[i].substr(0, results[i].lastIndexOf('/') + 1)
          let currentName = results[i].substr(results[i].lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
        } else {
          var currentName = results[i].substr(results[i].lastIndexOf('/') + 1)
          var correctPath = results[i].substr(0, results[i].lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
        }
      }
    }, error => {
      console.log(error)
    })
  }

  createNewNameForFileImage() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg'
    return newFileName
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.loadingPhotos = true
    this.file.copyFile(namePath, currentName, this.file.externalDataDirectory + 'images', newFileName)
      .then(success => {
        this.lastImage = newFileName
        let image = this.pathForImage(this.lastImage)
        this.requestImage(image)
        this.loadingPhotos = false
      }, error => {
        this.loadingPhotos = false
        debugger
      })
  }

  pathForImage(img) {
    if (img === null) {
      return ''
    } else {
      return this.file.externalDataDirectory + 'images/' + img
    }
  }

  requestImage(urlImage: string) {
    urlImage = this.win.Ionic.WebView.convertFileSrc(urlImage)
    this.listPhotos.push({
      route_image_original: urlImage,
      route_image_thumbnail: urlImage
    })
    this.group.controls[this.config.code].setValue(this.listPhotos)
  }

  openSheetPhoto() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Fotos',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Tomar foto',
          icon: !this.platform.is('ios') ? 'share' : null,
          handler: () => this.captureImage()
        },
        {
          text: 'Cargar foto',
          icon: !this.platform.is('ios') ? 'arrow-dropright-circle' : null,
          handler: () => this.uploadPhoto()
        },
        {
          text: 'Cancelar',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked')
            this.loadingPhotos = false
          }
        }
      ]
    })
    actionSheet.present()
  }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

}
