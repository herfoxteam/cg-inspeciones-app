import { Component, OnInit } from '@angular/core'
import { NavParams, ViewController, NavController, App } from 'ionic-angular'
import { FormPage } from '../../pages/form/form'
import { InspectionsProvider } from '../../providers/inspections/inspections'
import { Template } from '../template/template.model'
import { Schedule } from '../../pages/schedule/schedule.model'
import { Inspection } from '../../pages/inspections/inspection.model'
import { _ } from 'underscore'
import { TemplateProvider } from '../../providers/template/template'
import { ScheduleProvider } from '../../providers/schedule/schedule'

@Component({
  selector: 'preview-inspection',
  templateUrl: 'preview-inspection.html'
})

export class PreviewInspectionComponent implements OnInit {

  data: Template | Schedule
  disabledInspection: boolean = true
  buttonAction: string = 'Iniciar'
  actualInspection: Inspection

  constructor(
    private params: NavParams,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public appCtrl: App,
    private inspectionsProvider: InspectionsProvider,
    private templateProvider: TemplateProvider,
    private scheduleProvider: ScheduleProvider
  ) {
    this.data = params.data
    if (!this.data.structure) {
      this.templateProvider.getTemplateByIdOffline(this.data.template_id)
        .then((val: Template) => {
          let actualTemplate = val
          this.data.structure = actualTemplate.structure
        })
        .catch(error => {
          debugger
        })
    }
  }

  ngOnInit() {
    if (this.data.from === 'template')
      this.buttonAction = 'Iniciar'
    else if (this.inspectionIsStarted())
      this.buttonAction = 'Continuar'
  }

  dismiss(): void {
    this.viewCtrl.dismiss()
  }

  goToForm(idInspection: number): void {
    this.viewCtrl.dismiss()
    let viewTitle
    if (this.actualInspection)
      viewTitle = this.inspectionsProvider.getTitleInspection(this.actualInspection)

    this.appCtrl.getRootNav().push(FormPage, { idInspection, viewTitle })
  }

  prepareInspection(): void {
    if (this.inspectionIsStarted())
      this.loadInspection()
    else
      this.createInspectionOffline()
  }

  loadInspection(): void {
    let idInspection = this.data.inspection_id
    this.goToForm(idInspection)
  }

  createInspectionOffline(): void {
    let template_id
    if (this.data.from === 'template')
      template_id = this.data.id
    else
      template_id = this.data.template_id
    let offlineInspection: Inspection = {
      created_at: new Date(),
      expired_at: '',
      start_at: new Date(),
      updated_at: new Date(),
      id: new Date().getTime(),
      inspection_state_id: 1,
      schedules_code: null,
      template_id,
      user_id: JSON.parse(localStorage.getItem('currentUser')).id,
      user: JSON.parse(localStorage.getItem('currentUser')),
      group_assignment_id: null,
      scheduled: null,
      actions: [],
      images: [],
      observations: [],
      storage: null,
      template: this.data,
      state: {
        description: '',
        id: 2,
        title: 'in progress'
      },
      from: this.data.from,
      sync: false,
      stepsSyncOffline: {
        one: false,
        two: false,
        three: false
      }
    }
    if (this.data.from === 'schedule') {
      offlineInspection.scheduled = this.data
      offlineInspection.schedules_code = new Date().getTime().toString()
    }

    this.inspectionsProvider.createInspectionOffline(offlineInspection)
      .then((inspection: Inspection) => {
        this.actualInspection = inspection
        if (this.data.from === 'schedule') {
          this.scheduleProvider.updateScheduleOffline(this.data, inspection.id)
            .then(res => {
              this.goToForm(inspection.id)
            })
            .catch(error => {
              debugger
            })
        }
        else
          this.goToForm(inspection.id)
      })
      .catch(error => {
        debugger
      })
  }

  inspectionIsStarted(): boolean {
    return this.data.inspection_id !== null && this.data.inspection_id !== undefined
  }
}
