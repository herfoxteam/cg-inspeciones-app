import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { _ } from 'underscore'

@Component({
  selector: 'form-slider',
  template: `
    <div class="inspections__box" [formGroup]="group" id="{{config.code}}">
      <ion-list no-lines>
        <ion-item>
          <ion-label class="inspections__label">{{ config.storage.label }}</ion-label>
          <ion-range
            [min]="rangeParams.min"
            [max]="rangeParams.max"
            [step]="rangeParams.interval"
            snaps="true"
            pin="true"
            [formControlName]="config.code"
            name="value">
            <ion-label range-left>{{rangeParams.min}}</ion-label>
            <ion-label range-right>{{rangeParams.max}}</ion-label>
          </ion-range>
        </ion-item>
      </ion-list>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
  `
})

export class FormSliderComponent implements OnInit {

  config
  group: FormGroup
  rangeParams: any
  rangeValueActual: number

  constructor() { }

  ngOnInit() {
    this.rangeParams = this.config.storage.options_select
  }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

}
