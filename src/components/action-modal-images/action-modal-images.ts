import { Component, OnInit } from '@angular/core'
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker'
import { ActionSheetController, Platform, AlertController, ViewController, NavParams } from 'ionic-angular'
import { environment } from '../../environments/environment'
import { ActionsProvider } from '../../providers/actions/actions'
import { CameraOptions, Camera } from '@ionic-native/camera'
import { FilePath } from '@ionic-native/file-path'
import { File } from '@ionic-native/file'
import { _ } from 'underscore'
import { normalizeURL } from 'ionic-angular';

interface Image {
  item_code: number
  original_file: string,
  thumbnail: string,
  inspection_id: number
}

@Component({
  selector: 'action-modal-images',
  templateUrl: 'action-modal-images.html'
})
export class ActionModalImagesComponent implements OnInit {

  loadingPhotos: boolean = false
  images: Array<Image> = []
  urlBase = environment.urlBase
  code: any
  lastImage: string = null
  win: any = window

  constructor(
    private imagePicker: ImagePicker,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    public viewCtrl: ViewController,
    private params: NavParams,
    public alertCtrl: AlertController,
    public actionsProvider: ActionsProvider,
    public file: File,
    public filePath: FilePath,
    private camera: Camera
  ) { }

  ngOnInit() {
    this.code = this.params.data.code
    if (_.isArray(this.params.data.images))
      this.images = this.findImagesById(this.code, this.params.data.images)
  }

  captureImage() {
    let options: CameraOptions = {
      quality: 70,
      destinationType: 2,
      sourceType: 1,
      allowEdit: true,
      saveToPhotoAlbum: true,
      correctOrientation: true
    }
    this.camera.getPicture(options)
      .then(imagePath => {
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(imagePath)
            .then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1)
              let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1)
              this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
            })
        } else {
          let base64Image = null
          base64Image = normalizeURL(imagePath)
          console.log('BASE64IMAGE --- ' + base64Image)
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1)
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1)
          console.log('correctPath --- ' + correctPath)
          console.log('currentName --- ' + currentName)
          //this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
          //console.log('BASE64IMAGE --- ' + base64Image)

          this.requestImage(base64Image)
          //this.addPhotoTestIos(base64Image)
        }
      }, error => {
        debugger
      })
  }

  addPhotoTestIos(a) {
    debugger
    console.log('a --- ' + a)


  }

  uploadPhoto() {
    this.imagePicker.requestReadPermission()
    let options: ImagePickerOptions = {
      quality: 75,
      outputType: 0,
      width: 500
    }
    this.imagePicker.hasReadPermission()
      .then(res => {
        if (res)
          this.selectFromPickerImages(options)
        else {
          this.imagePicker.requestReadPermission()
            .then(res => {
              if (res === 'ok')
                this.selectFromPickerImages(options)
            })
        }
      })
      .catch(error => console.log(error))
  }

  selectFromPickerImages(options: ImagePickerOptions) {
    this.imagePicker.getPictures(options).then((results) => {
      for (let i = 0; i < results.length; i++) {
        if (this.platform.is('android')) {
          let correctPath = results[i].substr(0, results[i].lastIndexOf('/') + 1)
          let currentName = results[i].substr(results[i].lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
        } else {
          var currentName = results[i].substr(results[i].lastIndexOf('/') + 1)
          var correctPath = results[i].substr(0, results[i].lastIndexOf('/') + 1)
          this.copyFileToLocalDir(correctPath, currentName, this.createNewNameForFileImage())
        }
      }
    }, error => {
      console.log(error)
    })
  }

  createNewNameForFileImage() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg'
    return newFileName
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.loadingPhotos = true
    debugger
    console.log('namePath --- ' + namePath)
    console.log('currentName --- ' + currentName)
    console.log('newFileName --- ' + newFileName)

    this.file.copyFile(namePath, currentName, this.file.externalDataDirectory + 'images', newFileName)
      .then(success => {
        console.log('EXITOSO --- ')

        this.lastImage = newFileName
        let image = this.pathForImage(this.lastImage)
        this.requestImage(image)
        this.loadingPhotos = false
      }, error => {
        this.loadingPhotos = false
        console.log('ERROR --- ' + JSON.stringify(error))

        debugger
      })
  }

  pathForImage(img) {
    if (img === null) {
      return ''
    } else {
      return this.file.externalDataDirectory + 'images/' + img
    }
  }

  requestImage(urlImage: string) {
    this.loadingPhotos = true
    console.log('URLIMAGE --- ' + urlImage)

    urlImage = this.win.Ionic.WebView.convertFileSrc(urlImage)
    console.log('URLIMAGE2 --- ' + urlImage)

    urlImage = urlImage.replace("assets-library://", "cdvfile://localhost/assets-library/")

    let image = {
      item_code: this.code,
      original_file: urlImage,
      thumbnail: urlImage,
      inspection_id: JSON.parse(localStorage.getItem('inspectionId'))
    }
    this.actionsProvider.createImageOffline(image)
      .then(res => {
        this.images.push(image)
        this.loadingPhotos = false
      })
      .catch(error => {
        debugger
        this.loadingPhotos = false
      })
  }

  openSheetPhoto() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Fotos',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Tomar foto',
          icon: !this.platform.is('ios') ? 'share' : null,
          handler: () => this.captureImage()
        },
        {
          text: 'Cargar foto',
          icon: !this.platform.is('ios') ? 'arrow-dropright-circle' : null,
          handler: () => this.uploadPhoto()
        },
        {
          text: 'Cancelar',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked')
            this.loadingPhotos = false
          }
        }
      ]
    })
    actionSheet.present()
  }

  findImagesById(id: number, list: Array<any>): Array<any> {
    let result: any[] = []
    list.forEach(item => {
      if (item.item) {
        if (item.item.code === id.toString()) {
          result.push(item)
        }
      } else if (item.item_code === id) {
        result.push(item)
      }
    })
    return result
  }

  removeImage(image: Image) {
    let index = _.indexOf(this.images, image)
    if (index) {
      this.actionsProvider.deleteImageOffline(image)
        .then(val => {
          this.images.splice(index, 1)
        })
        .catch(error => {
          debugger
        })
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(this.images)
  }

}
