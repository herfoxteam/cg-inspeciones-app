import { Component, OnInit } from '@angular/core'
import { ViewController, NavParams } from 'ionic-angular'
import { _ } from 'underscore'
import { ActionsProvider } from '../../providers/actions/actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'action-modal-comments',
  templateUrl: 'action-modal-comments.html'
})

export class ActionModalCommentsComponent implements OnInit {

  comments: Array<any> = []
  code: any
  formObservation: FormGroup

  constructor(
    public viewCtrl: ViewController,
    private params: NavParams,
    public actionsProvider: ActionsProvider,
    private _formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.code = this.params.data.code

    this.formObservation = this._formBuilder.group({
      item_code: [this.code, Validators.required],
      inspection_id: [JSON.parse(localStorage.getItem('inspectionId')), Validators.required],
      description: ['', Validators.required]
    })
    if (_.isArray(this.params.data.observations)) {
      this.comments = this.findCommentsById(this.code, this.params.data.observations)
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(this.comments)
  }

  addComment() {
    if (this.formObservation.valid) {
      let comment = this.formObservation.value
      this.actionsProvider.createObservationOffline(comment)
        .then(val => {
          this.comments.push(comment)
        })
        .catch(error => {
          debugger
        })
      // this.actionsProvider.createObservation(comment)
      //   .subscribe(res => {
      //     this.comments.push(comment)
      //   }, res => {
      //     debugger
      //   })
      this.formObservation.controls.description.setValue('')
    }
  }

  findCommentsById(id: number, list: Array<any>): Array<any> {
    let result: any[] = []
    list.forEach(item => {
      if (item.item) {
        if (item.item.code === id.toString()) {
          result.push(item)
        }
      } else if (item.item_code === id) {
        result.push(item)
      }
    })
    return result
  }

}
