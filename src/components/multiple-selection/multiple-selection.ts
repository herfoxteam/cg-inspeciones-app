import { Component, OnInit } from '@angular/core'
import { NavParams, ViewController, NavController } from 'ionic-angular'
import { FormGroup } from '@angular/forms'

export interface OptionSelect {
  code: string
  value: string
  label: string
  checked: boolean
}

@Component({
  selector: 'multiple-selection',
  templateUrl: 'multiple-selection.html'
})

export class MultipleSelectionComponent implements OnInit {

  config: any
  group: FormGroup
  optionSelected: any
  typeSelection: string
  options: Array<OptionSelect>

  constructor(
    private params: NavParams,
    public viewCtrl: ViewController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.config = this.params.data.config
    this.group = this.params.data.group
    this.typeSelection = this.config.storage.type
    this.options = this.config.storage.items
    let responses = this.group.controls[this.config.code].value

    if (responses && responses !== '') {
      this.options.forEach(item => {
        responses.forEach(res => {
          if (item.code === res.code)
            item.checked = res.checked
        })
      })
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(this.options)
  }

  changeSelection(option, event) {
    if (this.typeSelection === 'unique') {
      if (this.getQuantitySelectedOptions() > 1) {
        this.cleanSelection()
        option.checked = true
      }
    }
  }

  cleanSelection() {
    if (this.options) {
      this.options.forEach(item => item.checked = false)
    }
  }

  getQuantitySelectedOptions(): number {
    if (this.options) {
      let result: any = []
      this.options.forEach(opt => {
        if (opt.checked) {
          result.push(opt)
        }
      })
      return result.length
    }
    return null
  }
}
