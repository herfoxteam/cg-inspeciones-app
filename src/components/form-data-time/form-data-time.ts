import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { _ } from 'underscore'

@Component({
  selector: 'form-data-time',
  template: `
    <div class="inspections__box" [formGroup]="group" id="{{config.code}}">
      <ion-list no-lines>
        <p class="inspections__label">{{config.storage?.label}}</p>
        <ion-item class="date">
          <ion-datetime
            displayFormat="DD/MM/YYYY"
            max="2030"
            [formControlName]="config.code">
          </ion-datetime>
        </ion-item>
      </ion-list>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
  `
})

export class FormDataTimeComponent implements OnInit {

  config
  group: FormGroup

  constructor() { }

  ngOnInit() { }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

}
