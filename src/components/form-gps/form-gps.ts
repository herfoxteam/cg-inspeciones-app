import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { Geolocation } from '@ionic-native/geolocation'
import { _ } from 'underscore'

@Component({
  selector: 'form-gps',
  template: `
    <div class="dynamic-field form-input inspections__box" [formGroup]="group" id="{{config.code}}">
      <p class="inspections__label">{{ config.storage.label }}</p>
      <ion-input [formControlName]="config.code" style="display:none"></ion-input>
      <div class="data">{{valueShow}}</div>
      <button color="primary" ion-button (click)="getGeolocation()">
        <ion-icon name="locate"></ion-icon>
        Obtener ubicación actual
      </button>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
  `
})

export class FormGpsComponent implements OnInit {

  config
  group: FormGroup
  valueShow: string

  constructor(
    private geolocation: Geolocation
  ) { }

  ngOnInit() {
    let response = this.group.controls[this.config.code].value
    this.valueShow = `Latitud: ${response[0]} - Longitud:  ${response[1]}`
  }

  // showData(): boolean {
  //   let response = this.group.controls[this.config.code].value
  //   debugger
  //   if(response !== '')
  //     return false
  //   else
  //     return true
  // }

  getGeolocation() {
    this.geolocation.getCurrentPosition().then(resp => {
      this.valueShow = `Latitud: ${resp.coords.latitude} - Longitud:  ${resp.coords.longitude}`
      this.group.controls[this.config.code].setValue([resp.coords.latitude, resp.coords.longitude])
    }).catch((error) => {
      console.log('Error getting location', error)
    })
  }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

}
