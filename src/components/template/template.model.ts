export interface Template {
	id: number
	title: string
	description: string
	code: string
	structure: string
	user_id: number
	published: boolean
	created_at: string
	updated_at: string
	from?: string
	state?: string
	inspection_id?: number
	template_id?: number
}