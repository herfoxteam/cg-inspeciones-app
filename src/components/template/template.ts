import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Inspection } from '../../pages/inspections/inspection.model'
import { InspectionsProvider } from '../../providers/inspections/inspections'
import { LoadingController, AlertController, ModalController, NavController, Platform, Content } from 'ionic-angular'
import { _ } from 'underscore'
import { InspectionAlertComponent } from '../inspection-alert/inspection-alert'
import { TemplateProvider } from '../../providers/template/template';
import { Template } from './template.model';

@Component({
  selector: 'inspection-template',
  template: `
    <ion-content>
      <form [formGroup]="rootForm">
        <ng-container *ngFor="let field of configForm" dynamicField [config]="field" [group]="rootForm" [rootConfig]="categories">
        </ng-container>
        <button class="finish" type="submit" (click)="finishInspection($event)" ion-button full color="primary">Finalizar y enviar</button>
        <button class="save" type="submit" (click)="saveInspection($event)" ion-button full color="primary">Guardar</button>
      </form>
    </ion-content>
  `
})

export class TemplateComponent implements OnInit {

  @Input() config: number
  @ViewChild(Content) content: Content
  configForm: any[] = []
  rootForm: FormGroup
  categories: any[] = []
  loaderSaveInspection: any
  loaderFinishInspection: any
  loaderGetInspection: any
  showAlertMessage = true
  questions = []
  actualInspection: any

  constructor(
    private fb: FormBuilder,
    private inspectionsProvider: InspectionsProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public platform: Platform,
    private templateProvider: TemplateProvider
  ) { }

  ngOnInit() {
    localStorage.setItem('showAlert', 'true')
    this.rootForm = new FormGroup({})
    this.questions = []
    localStorage.setItem('inspectionId', `${this.config}`)

    this.getInspectionOffline()
  }

  getInspectionOffline() {
    this.inspectionsProvider.getInspectionsOffline()
      .then((val) => {
        let inspections = JSON.parse(val) || []
        this.actualInspection = _.findWhere(inspections, { id: this.config })
        if (this.actualInspection) {
          if (!this.actualInspection.template.structure) {
            this.templateProvider.getTemplateByIdOffline(this.config)
              .then((res: Template) => {
                this.rootForm = this.getCategories(JSON.parse(res.structure))
              })
              .catch(error => {
                debugger
              })
          }
          else
            this.rootForm = this.getCategories(JSON.parse(this.actualInspection.template.structure))
          this.loadResponsesInspectionsToForm(this.actualInspection)
          this.loadAttachmentsInspectionsToForm(this.actualInspection)
        }
      })
      .catch(error => {
        debugger
      })
  }

  saveInspection(from?: string) {
    this.requestInspection('save', 'loaderSaveInspection', from)
  }

  finishInspection() {
    if (this.verifyForm())
      this.requestInspection('terminate', 'loaderFinishInspection')
    else {
      this.buildErrors(true)
      const alert = this.alertCtrl.create({
        subTitle: 'Error al guardar',
        message: 'Verifique las respuestas por favor'
      })
      alert.present()
    }
  }

  requestInspection(type: 'terminate' | 'save', typeLoading: string, from?: string) {
    this.presentLoading(typeLoading, 'guardando')
    let actualValues = this.rootForm.value
    let responsesInspection = this.buildRequestInspection(actualValues)

    this.inspectionsProvider.saveInspectionOffline(this.config, responsesInspection, type)
      .then(val => {
        this[`${typeLoading}`].dismiss()
        this.afterRequestInspection(responsesInspection, type, from)
      })
      .catch(error => {
        this[`${typeLoading}`].dismiss()
        if (type === 'save') {
          const alert = this.alertCtrl.create({
            subTitle: 'Error al guardar',
            message: 'Verifique las respuestas por favor'
          })
          alert.present()
        }
      })
  }

  buildRequestInspection(actualValues: any) {
    let data: Array<any> = this.buildResponsesInspections(actualValues)
    let { actions, observations, images }: { actions: Array<any>; observations: Array<any>; images: Array<any>; } = this.buildActionsInspections(actualValues)

    let responses = {
      data: JSON.stringify(data),
      actions: JSON.stringify(actions),
      observations: JSON.stringify(observations),
      images: JSON.stringify(images)
    }
    return responses
  }

  buildResponsesInspections(actualValues: any) {
    let data: Array<any> = []
    for (const prop in actualValues) {
      if (actualValues[prop] !== '' && actualValues[prop] !== undefined && prop !== 'actions' && (actualValues[prop].length > 0) || _.isNumber(actualValues[prop]) || _.isBoolean(actualValues[prop])) {
        data.push({
          code: prop,
          response: actualValues[prop]
        })
      }
    }
    return data
  }

  buildActionsInspections(actualValues: any) {
    let actions: Array<any> = []
    let observations: Array<any> = []
    let images: Array<any> = []
    if (_.isObject(actualValues.actions) && (!_.isArray(actualValues.actions))) {
      if (_.isArray(actualValues.actions.observations)) {
        actualValues.actions.observations.forEach(item => {
          observations.push(item)
        })
      }
      if (_.isArray(actualValues.actions.actions)) {
        actualValues.actions.actions.forEach(item => {
          actions.push(item)
        })
      }
      if (_.isArray(actualValues.actions.images)) {
        actualValues.actions.images.forEach(item => {
          images.push(item)
        })
      }
    }
    return { actions, observations, images }
  }

  findStructureByCategoryValue(value: number) {
    let result
    this.categories.forEach(item => {
      if (item.category === `category_${value}`)
        result = item
    })
    if (result)
      return result
    else
      return []
  }

  presentLoading(loader: string, text?: string) {
    this[loader] = this.loadingCtrl.create({
      content: `${text}...`
    })
    this[loader].present()
  }

  verifyForm() {
    let result: boolean = false
    let actualValues = this.rootForm.value
    for (const prop in actualValues) {
      if (prop == 'actions')
        result = true
      else if (actualValues[prop] !== '' && actualValues[prop] !== undefined && (actualValues[prop].length > 0) || _.isNumber(actualValues[prop]) || _.isBoolean(actualValues[prop])) {
        result = true
      }
      else {
        result = false
        break
      }
    }
    return result
  }

  buildErrors(scroll: boolean) {
    let errors = []
    if (scroll) {
      for (const prop in this.rootForm.controls) {
        if (this.rootForm.controls[prop].value !== '' && this.rootForm.controls[prop].value !== undefined && (this.rootForm.controls[prop].value.length > 0) || _.isNumber(this.rootForm.controls[prop].value) || _.isBoolean(this.rootForm.controls[prop].value)) {
          debugger
        } else if (prop === 'actions') {
          debugger
        } else {
          debugger
          errors.push(prop)
        }
      }
      if (errors.length > 0)
        this.scrollTo(errors[0])
    }
  }

  scrollTo(elementId: string) {
    let y = document.getElementById(elementId).offsetTop
    console.log(elementId)
    this.content.scrollTo(0, y - 80, 2000)
  }

  getCategories(structure: any): FormGroup {
    this.categories = []
    for (const prop in structure) {
      let category = structure[prop]
      category.category = prop
      this.categories.push(category)
    }
    this.configForm = this.categories[0]
    const group = this.fb.group({})
    this.buildTemplate(this.configForm, group)
    return group
  }

  loadAttachmentsInspectionsToForm(res: Inspection) {
    let actions = res.actions
    let images = res.images
    let observations = res.observations
    this.rootForm.controls['actions'].setValue({ actions, images, observations })
  }

  loadResponsesInspectionsToForm(res: Inspection) {
    if (res.storage !== null) {
      let responses
      if (_.isString(res.storage))
        responses = JSON.parse(res.storage)
      else
        responses = res.storage

      let data
      _.isString(responses.data) ? data = JSON.parse(responses.data) : []
      if (!_.isArray(data)) {
        _.isString(JSON.parse(responses.data).data) ? data = JSON.parse(JSON.parse(responses.data).data) : []
      }
      data.forEach(item => {
        this.rootForm.controls[item.code].setValue(item.response)
      })
    }
  }

  buildTemplate(list: any[], group: FormGroup) {
    list.forEach(ctrl => {
      // if (ctrl.type === 'category') return
      if (ctrl.type === 'component') {
        this.questions.push(ctrl)
        let validations: Array<any> = this.buildValidationsArray(ctrl)

        if (ctrl.value === 'switch')
          group.addControl(ctrl.code, this.fb.control(false, validations))
        else if (ctrl.value === 'slider')
          group.addControl(ctrl.code, this.fb.control(ctrl.storage.options_select.min, validations))
        else
          group.addControl(ctrl.code, this.fb.control('', validations))
      }
      else if (ctrl.type === 'category') {
        let structure = this.findStructureByCategoryValue(ctrl.value)
        this.buildTemplate(structure, group)
      }
    })
    group.addControl('actions', this.fb.control([]))
  }

  buildValidationsArray(ctrl: any): Array<any> {
    let validations: Array<any> = []
    if (ctrl.storage) {
      if (ctrl.storage.required === true) {
        validations.push(Validators.required)
      }
      if (ctrl.value === 'text' || ctrl.value === 'textarea') {
        if (ctrl.storage.characters) {
          let characters = ctrl.storage.characters
          validations.push(Validators.minLength(characters[0]))
          validations.push(Validators.maxLength(characters[1]))
        }
      }
      if (ctrl.value === 'slider') {
        if (ctrl.storage.options_select) {
          validations.push(Validators.min(ctrl.storage.options_select.min))
          validations.push(Validators.min(ctrl.storage.options_select.max))
        }
      }
    }
    return validations
  }

  afterRequestInspection(responses: { data: string; actions: string; observations: string; images: string; }, type: string, from: string) {
    let pdfResponses = this.buildQuestionsAndAnswersPDF(JSON.parse(responses.data));
    if (type === 'save') {
      if (from !== 'confirm') {
        let viewModal = this.modalCtrl.create(InspectionAlertComponent, { pdfResponses: null, inspection: this.actualInspection }, { cssClass: 'modal__alert' });
        viewModal.present();
      }
      else {
        this.navCtrl.pop({});
      }
    }
    else if (type === 'terminate') {
      let viewModal = this.modalCtrl.create(InspectionAlertComponent, { pdfResponses, inspection: this.actualInspection }, { cssClass: 'modal__alert' });
      viewModal.present();
      viewModal.onDidDismiss(res => {
        if (res) {
          localStorage.setItem('showAlert', 'false');
          this.navCtrl.pop();
        }
      });
    }
  }

  buildQuestionsAndAnswersPDF(responses: Array<any>): Array<any> {
    let questions: Array<any> = this.questions
    responses.forEach(response => {
      let result = _.findWhere(questions, { code: parseInt(response.code) })
      if (result) {
        if (result.value === 'checkbox') {
          let temp = response.response.filter(item => item.code)
          let res = []
          result.storage.items.forEach(opt => {
            temp.forEach(val => {
              opt.code === val.code ? res.push(opt.label) : null
            })
          })
          response.response = res
        }
        result = result.storage.label
      }
      response.question = result
    })
    return responses
  }
}
