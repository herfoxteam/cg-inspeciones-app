import { Component } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { _ } from 'underscore'

@Component({
  selector: 'form-textarea',
  template: `
    <div class="dynamic-field form-input inspections__box" [formGroup]="group" id="{{config.code}}">
      <p class="inspections__label">{{ config.storage.label }}</p>
      <ion-textarea [formControlName]="config.code"></ion-textarea>
      <attachments [code]="config.code" [actions]="group" (changeActions)="appendActions($event)"></attachments>
    </div>
  `
})

export class FormTextareaComponent {

  config
  group: FormGroup

  constructor() { }

  appendActions(event) {
    let data = this.group.controls['actions'].value
    let joinObservations = _.union(event.observations, data.observations)
    let joinImages = _.union(event.images, data.images)
    let joinActions = _.union(event.actions, data.actions)
    let actions = {
      actions: joinActions,
      observations: joinObservations,
      images: joinImages
    }
    this.group.controls['actions'].setValue(actions)
  }

}
