import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms'

interface Category {
  id: number
  code: number
  active: boolean
  data: string
  errors: boolean
  img: string
  storage: { label: string },
  type: string
  value: number
}

@Component({
  selector: 'form-category',
  template: `
    <expandable [expanded]="itemExpanded" [title]="titleCategory">
      <ng-container *ngFor="let field of configForm" dynamicField [config]="field" [group]="rootForm">
      </ng-container>
    </expandable>
  `
})

export class FormCategoryComponent implements OnInit {
  config: Category
  group: FormGroup
  rootConfig: any[]

  configForm: any[] = []
  rootForm: FormGroup
  itemExpanded: boolean = true
  titleCategory: string

  constructor( ) { }

  ngOnInit() {
    this.titleCategory = this.config.storage.label
    this.configForm = this.findStructureByCategoryValue(this.config.value)
    this.rootForm = this.group
  }

  findStructureByCategoryValue(value: number) {
    let result
    this.rootConfig.forEach(item => {
      if (item.category === `category_${value}`)
        result = item
    })
    return result
  }

}
