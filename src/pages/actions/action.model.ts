export interface Action extends ActionOffline {
	id: number
	description: string
	date_time_end: Date
	assigned_user_id: number
	item_id?: number
	closed: boolean
	priority: string
	inspection_id: number
	created_at: string | Date
	updated_at: string | Date
	states_actions_id: number
	code?: any
	inspection?: any
	assigned: any
	state: {
		id: number,
		title: string
		description: string
	}
	item?: any
	item_code?: any,
	sync: boolean
}

interface ActionOffline {
	comments?: Array<any>
}