import { Component } from '@angular/core'
import { NavController, NavParams, LoadingController } from 'ionic-angular'
import { ActionsProvider } from '../../providers/actions/actions'
import { Action } from './action.model'
import { ActionDetailPage } from '../action-detail/action-detail'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection';

@Component({
  selector: 'page-actions',
  templateUrl: 'actions.html',
})

export class ActionsPage {

  actions: Action[] = []
  myActions: Action[] = []
  searchText: string = ''
  filterAction: string = 'assigned'
  notActions: boolean
  notMyActions: boolean
  loader: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private actionsProvider: ActionsProvider,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private connectionProvider: ConnectionProvider
  ) { }

  ionViewWillEnter() {
    this.getAllActions()
    this.getMyActions()
  }

  syncAllActions() {
    this.loader = this.loadingCtrl.create({
      content: 'cargando...'
    })
    this.actionsProvider.getActions()
      .subscribe(res => {
        let listActionsServer = res.data
        this.actionsProvider.getActionsOffline()
          .then((val: string) => {
            let listActionsOffline =  JSON.parse(val) || []
            return this.actionsProvider.syncAllActions(listActionsOffline, listActionsServer)
          })
          .then((actions: Action[]) => {
            this.actions = actions
            this.validateActions()
            this.loader.dismiss()
          })
          .catch(error => {
            debugger
            this.validateActions()
            this.loader.dismiss()
          })
      }, error => {
        this.loader.dismiss()
        debugger
      })
  }

  getAllActions(event?) {
    if (this.connectionProvider.isConnected()) {
      this.syncAllActions()
      this.syncMyActions()
    }

    this.actionsProvider.getActionsOffline()
      .then((val) => {
        this.actions = JSON.parse(val) || []
        this.validateActions()
        event ? event.complete() : null
      })
      .catch(error => {
        event ? event.complete() : null
        debugger
      })
  }

  validateActions() {
    if (this.actions)
      this.actions.length > 0 ? this.notActions = false : this.notActions = true
    else
      this.notActions = true
  }

  syncMyActions() {
    this.actionsProvider.getMyActions()
      .subscribe(res => {
        this.storage.set('myActions', JSON.stringify(res.data))
        this.myActions = res.data
        this.validateMyActions();
      }, error => { })
  }

  getMyActions(event?) {
    if (this.connectionProvider.isConnected())
      this.syncMyActions()

    this.actionsProvider.getMyActionsOffline()
      .then((val) => {
        this.myActions = JSON.parse(val) || []
        this.validateMyActions()
        event ? event.complete() : null
      })
      .catch(error => {
        event ? event.complete() : null
        debugger
      })
  }

  validateMyActions() {
    if (this.myActions)
      this.myActions.length > 0 ? this.notMyActions = false : this.notMyActions = true
    else
      this.notMyActions = true
  }

  goToActionDetail(id: number) {
    this.navCtrl.push(ActionDetailPage, id)
  }

}
