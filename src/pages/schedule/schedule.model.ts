export interface Schedule {
	id: number
	description: string
	start: Date
	end: Date
	state: string
	title: string
	from?: string
	inspection_id?: number
	structure?: string,
	template_id?: number
}