import { Component } from '@angular/core'
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular'
import { ScheduleProvider } from '../../providers/schedule/schedule'
import { PreviewInspectionComponent } from '../../components/preview-inspection/preview-inspection'
import { Schedule } from './schedule.model'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection'
import { _ } from 'underscore'

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})

export class SchedulePage {

  scheduled: Schedule[] = []
  notScheduled: boolean
  loader: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private scheduleProvider: ScheduleProvider,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private connectionProvider: ConnectionProvider
  ) { }

  ionViewWillEnter() {
    this.getSchedules()
  }

  syncSchedules() {
    this.loader = this.loadingCtrl.create({
      content: 'cargando...'
    })
    this.loader.present()
    this.scheduleProvider.getSchedule()
      .subscribe(res => {
        let listScheduleServer = res.data
        this.storage.get('schedule')
          .then((res: Schedule[]) => {
            return res
          })
          .then((listScheduleOffline: Schedule[]) => {
            return this.scheduleProvider.syncSchedule(listScheduleOffline, listScheduleServer)
          })
          .then((scheduled: Schedule[]) => {
            this.scheduled = scheduled
            this.loader.dismiss()
            this.validateScheduled()
          })
          .catch(error => {
            debugger
            this.validateScheduled()
            this.loader.dismiss()
          })
      }, error => {
        this.loader.dismiss()
      })
  }

  validateNewsSchedule(listSchedule: Schedule[]) {
    listSchedule.forEach((item: Schedule) => {
      let resultado = this.scheduled.find((sch: Schedule) => sch.id === item.id)
      if (!resultado)
        this.scheduled.push(item)
    })
  }

  getSchedules(event?) {
    if (this.connectionProvider.isConnected())
      this.syncSchedules()

    this.scheduleProvider.getScheduleOffline()
      .then((val) => {
        this.scheduled = JSON.parse(val) || []
        this.validateScheduled()
        event ? event.complete() : null
      })
      .catch(error => {
        event ? event.complete() : null
        debugger
      })
  }

  validateScheduled() {
    if (this.scheduled)
      this.scheduled.length > 0 ? this.notScheduled = false : this.notScheduled = true;
    else
      this.notScheduled = true
  }

  openPreviewInspection(schedule: Schedule) {
    schedule.from = 'schedule'
    let modal = this.modalCtrl.create(PreviewInspectionComponent, schedule, { cssClass: 'modal__preview' })
    modal.present()
  }

}
