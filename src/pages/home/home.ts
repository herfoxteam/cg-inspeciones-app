import { Component } from '@angular/core'
import { NavController, LoadingController, ModalController } from 'ionic-angular'
import { ActionsProvider } from '../../providers/actions/actions'
import { Action } from '../actions/action.model'
import { ScheduleProvider } from '../../providers/schedule/schedule'
import { SchedulePage } from '../schedule/schedule'
import { Schedule } from '../schedule/schedule.model'
import { PreviewInspectionComponent } from '../../components/preview-inspection/preview-inspection'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection';
import { SyncAllProvider } from '../../providers/sync-all/sync-all';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  actions: Action[] = []
  scheduled: Array<any> = []
  connection: boolean
  loader: any
  notScheduled: boolean
  notActions: boolean

  constructor(
    public navCtrl: NavController,
    private actionsProvider: ActionsProvider,
    private scheduleProvider: ScheduleProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private connectionProvider: ConnectionProvider,
    private syncAllProvider: SyncAllProvider,
    private authProvider: AuthProvider
  ) { }

  ionViewWillEnter() {
    this.getScheduleToday()
    this.getActionsByDays()
  }

  refresher(event) {
    this.getActionsByDays()
    this.getScheduleToday()

    if (this.connectionProvider.isConnected()) {
      this.syncScheduleToday()
      this.syncActionsByDays()
    }
    event.complete()
  }

  /*
   * ------- ACTIONS --------
   */
  syncActionsByDays() {
    this.actionsProvider.getActionsByDays(3)
      .subscribe(res => {
        this.storage.set('actionsByDays', JSON.stringify(res.data))
        this.actions = res.data
        this.validateActionsByDays()
      }, error => { })
  }

  getActionsByDays() {
    this.storage.get('actionsByDays')
      .then((val) => {
        this.actions = JSON.parse(val) || []
        this.actions.length > 0 ? this.notActions = false : this.notActions = true
      })
      .catch(error => {
        debugger
      })
  }

  validateActionsByDays() {
    if(this.actions)
      this.actions.length > 0 ? this.notActions = false : this.notActions = true
    else
      this.notActions = true
  }

  /*
   * ------- SCHEDULE --------
   */
  syncScheduleToday() {
    this.loader = this.loadingCtrl.create({
      content: 'cargando...'
    })
    this.loader.present()
    this.scheduleProvider.getScheduleToday()
      .subscribe(res => {
        this.loader.dismiss()
        this.storage.set('scheduledToday', JSON.stringify(res.data))
        this.scheduled = res.data
        this.validateSchedule()
      }, error => {
        debugger
        this.loader.dismiss()
      })
  }

  getScheduleToday() {
    this.scheduleProvider.getScheduleTodayOffline()
      .then((val) => {
        this.scheduled = JSON.parse(val) || []
        this.validateSchedule()
      })
      .catch(error => {
        debugger
      })
  }

  validateSchedule() {
    if(this.scheduled)
      this.scheduled.length > 0 ? this.notScheduled = false : this.notScheduled = true
    else
      this.notScheduled = true
  }

  getAllSchedules() {
    this.navCtrl.setRoot(SchedulePage)
  }

  /*
   * ------- OTHERS --------
   */

  openPreviewInspection(schedule: Schedule) {
    schedule.from = 'schedule'
    let modal = this.modalCtrl.create(PreviewInspectionComponent, schedule, { cssClass: 'modal__preview' })
    modal.present()
  }

}
