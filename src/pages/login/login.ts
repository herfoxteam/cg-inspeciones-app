import { Component, OnInit, ViewChild } from '@angular/core'
import { NavController, NavParams, AlertController, Nav, LoadingController, ModalController, Platform } from 'ionic-angular'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthProvider } from '../../providers/auth/auth'
import { HomePage } from '../home/home'
import { Storage } from '@ionic/storage'
import { ModalTermsComponent } from '../../components/modal-terms/modal-terms'
import { FcmProvider } from '../../providers/fcm/fcm'
import { SyncAllProvider } from '../../providers/sync-all/sync-all';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage implements OnInit {

  formLogin: FormGroup
  loader: any
  @ViewChild(Nav) nav: Nav

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _formBuilder: FormBuilder,
    private authProvider: AuthProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public modalCtrl: ModalController,
    private fcmProvider: FcmProvider,
    private syncAllProvider: SyncAllProvider,
    private fb: Facebook,
    public platform: Platform
  ) {
  }

  ngOnInit() {
    this.formLogin = this._formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', Validators.required],
      terms: [null, Validators.required]
    })
  }

  login(fb?: boolean) {
    debugger
    if(fb) {
      let data = {
        username: 'hforigua',
        password: 'hforigua',
        terms: true
      }
      this.requestLogin(data)
    }
    else {
      if (this.formLogin.valid)
        this.requestLogin(this.formLogin.value)
      else
        this.buildErrors()
    }
    
  }

  requestLogin(data) {
    console.log('Logged into Facebook! noo', data) 
    debugger
    
    this.presentLoading('cargando')
    this.authProvider.login(data)
      .subscribe(res => {
        this.storage.set('token', res.access_token)
          .then(res => {
            this.authProvider.verifyAuth()
              .subscribe(res => {
                localStorage.setItem('currentUser', JSON.stringify(res))
                this.loader.dismiss()
                this.syncAllDataPostLogin()
              })
          }, error => { this.loader.dismiss() })
      }, error => { this.loader.dismiss() })
  }

  syncAllDataPostLogin() {
    this.presentLoading('sincronizando su información...')
    this.fcmProvider.getToken()
      .then(() => this.syncAllProvider.syncInspections())
      .then((inspections: string) => this.syncAllProvider.syncAllData())
      .then(() => {
        this.loader.dismiss()
        this.navCtrl.setRoot(HomePage)
      })
      .catch((error) => {
        console.log(error)
        this.loader.dismiss()
      })
  }

  loginFb(e) {
    this.fb.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        let userId = res.authResponse.userID

        this.fb.api(userId + '/?fields=id,email,first_name,last_name', ['public_profile'])
          .then(res => {
            let data = {
              name: `${res.first_name} ${res.last_name}`,
              username: `${res.first_name}${res.last_name}`,
              email: res.email,
              password: 'facebook'
            }
            console.log('data:' + data.username)
            debugger
            this.formLogin.controls.username.setValue(data.username)
            this.formLogin.controls.password.setValue('facebook')
            this.formLogin.controls.terms.setValue(true)
            console.log('set values:' + this.formLogin.value)
            this.login(true)
          }, error => {
            debugger
          })
      })
      .catch(e => {
        console.log('Error logging into Facebook', e)
      })
  }

  buildErrors() {
    let errors = ''
    for (const prop in this.formLogin.controls) {
      if (!this.formLogin.controls[prop].valid) {
        errors += `${prop} `
      }
    }
    let alert = this.alertCtrl.create({
      subTitle: `Por favor verifique ${errors}`,
      buttons: ['OK']
    })
    alert.present()
  }

  openTermsConditions() {
    const modal = this.modalCtrl.create(ModalTermsComponent, {}, { cssClass: 'modal__terms' })
    modal.present()
  }

  presentLoading(text?: string) {
    this.loader = this.loadingCtrl.create({
      content: `${text}...`
    })
    this.loader.present()
  }

}
