import { Component, ViewChild, OnInit } from '@angular/core'
import { NavController, NavParams, Nav, Platform, AlertController } from 'ionic-angular'
import { TemplateComponent } from '../../components/template/template'
import { Inspection } from '../inspections/inspection.model'

@Component({
  selector: 'page-form',
  template: `
    <ion-header>
      <ion-navbar>
        <ion-title *ngIf="!config.scheduled">{{viewTitle}}</ion-title>
      </ion-navbar>
    </ion-header>
    <inspection-template #inspectionTemplate [config]="config"></inspection-template>
  `
})

export class FormPage implements OnInit {

  @ViewChild(Nav) nav: Nav
  @ViewChild('inspectionTemplate') inspectionTemplate

  config: Number
  viewTitle: string
  showAlertMessage = true

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public platform: Platform,
    public alertCtrl: AlertController
  ) {
    this.config = params.data.idInspection
    this.viewTitle = params.data.viewTitle
  }

  ionViewCanLeave() {
    if (localStorage.getItem('showAlert') !== 'false') {
      localStorage.setItem('showAlert', 'true')
      let view = this.navCtrl.getActive()
      if (view.component.name === 'FormPage' && this.showAlertMessage) {
        this.alertExitConfirm()
        return false
      }
    }
  }

  ngOnInit() {
    if (localStorage.getItem('showAlert') !== 'false') {
      localStorage.setItem('showAlert', 'true')
      this.platform.registerBackButtonAction(() => {
        let view = this.navCtrl.getActive()
        if (view.component.name === 'FormPage' && this.showAlertMessage) {
          this.alertExitConfirm()
        }
      })
    }
  }

  alertExitConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirmación',
      message: '¿Desea salir de esta inspección sin guardar cambios?',
      buttons: [
        {
          text: 'Descartar cambios',
          role: 'cancel',
          handler: () => {
            this.showAlertMessage = false
            this.navCtrl.pop({})
          }
        },
        {
          text: 'Guardar',
          handler: () => {
            this.inspectionTemplate.saveInspection('confirm')
            this.showAlertMessage = false
          }
        }
      ]
    })
    alert.present()
  }

}
