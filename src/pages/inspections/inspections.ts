import { Component } from '@angular/core'
import { NavController, NavParams, ModalController, LoadingController, Platform, AlertController } from 'ionic-angular'
import { Inspection } from './inspection.model'
import { InspectionsProvider } from '../../providers/inspections/inspections'
import { FormPage } from '../form/form'
import { FormControl } from '@angular/forms'
import { _ } from 'underscore'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection';

@Component({
  selector: 'page-inspections',
  templateUrl: 'inspections.html',
})

export class InspectionsPage {

  inspections: Array<Inspection> = []
  inspectionsClone: Array<any> = []
  notInspections: boolean
  loader: any
  search: FormControl = new FormControl('')
  searchText: string = ''

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private inspectionsProvider: InspectionsProvider,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public alertCtrl: AlertController,
    private storage: Storage,
    private connectionProvider: ConnectionProvider
  ) { }

  ionViewWillEnter() {
    this.getInspections()
  }

  clear() {
    this.storage.remove('inspections')
  }

  clearSchedule() {
    this.storage.remove('schedule')
  }

  getInspections(event?) {
    this.inspectionsProvider.getInspectionsOffline()
      .then((val) => {
        this.inspections = JSON.parse(val) || []
        this.validateInspections()
        event ? event.complete() : null
      })
      .catch(error => {
        event ? event.complete() : null
        console.log(error)
      })
  }

  validateInspections() {
    if (this.inspections)
      this.inspections.length > 0 ? this.notInspections = false : this.notInspections = true
    else
      this.notInspections = true
  }

  goToForm(inspection: Inspection) {
    let viewTitle = this.inspectionsProvider.getTitleInspection(inspection)
    this.navCtrl.push(FormPage, { idInspection: inspection.id, viewTitle })
  }

}
