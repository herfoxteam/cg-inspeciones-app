import { Action } from "../actions/action.model";

export interface Inspection extends InspectionOffline {
	id: number
	template_id: number
	user_id: number
	created_at: string | Date
	updated_at: string | Date
	deleted_at?: any | Date
	expired_at: string | Date
	start_at: string | Date
	schedules_code: string
	group_assignment_id: number
	inspection_state_id: number
	state: State
	user: User
	scheduled: Scheduled
	template: Template
	from: string
}

interface InspectionOffline {
	actions?: Array<any>
	images?: Array<any>
	observations?: Array<any>
	storage?: any
	sync: boolean
	stepsSyncOffline?: {
		one: boolean,
		two: boolean,
		three: boolean
	}
}

interface Template {
	title: string
}

interface Scheduled {
	id?: number
	title: string
	observations?: any
}

interface User {
	id: number
	name: string
	photo: string
	email: string
	id_cms_privileges: number
	created_at: string | Date
	updated_at?: any
	status: string
	username: string
}

interface State {
	id: number
	title: string
	description: string
}

export interface CreateInspectionFromTemplate {
	template_id: number
	date_time_start: Date | string
	from?: string
}

export interface CreateInspectionFromSchedule {
	event_id: number
	date_time_start: Date | string
	from?: string
}