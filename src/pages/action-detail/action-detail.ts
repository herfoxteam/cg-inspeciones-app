import { Component, OnInit } from '@angular/core'
import { NavController, NavParams, ModalController, Platform, AlertController, ActionSheetController, LoadingController } from 'ionic-angular'
import { ActionsProvider } from '../../providers/actions/actions'
import { Action } from '../actions/action.model'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { CreateAction } from '../../components/action-modal-actions/create-action'
import { _ } from 'underscore'

@Component({
  selector: 'page-action-detail',
  templateUrl: 'action-detail.html',
})

export class ActionDetailPage implements OnInit {

  actionId: number
  action: Action
  actionFromBack: Action
  formComment: FormGroup = this._formBuilder.group({
    action_id: ['', Validators.required],
    body: ['', Validators.required],
    created_at: [new Date(), Validators.required]
  })
  comments: Array<any> = []
  currentUser: any
  actionState: string
  loaderAction: any
  unlockEditAction: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private actionsProvider: ActionsProvider,
    private _formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public platform: Platform,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.actionId = this.navParams.data.id
    this.actionFromBack = this.navParams.data
    this.validateShowEditButton(this.actionFromBack.assigned.id)
  }

  ionViewWillEnter() {
    this.inicializeFormComment()
    this.getActionById()
  }

  getActionById() {
    this.loaderAction = this.loadingCtrl.create({
      content: `cargando ...`
    })
    if (_.isNumber(this.actionId)) {
      this.loaderAction.present()
      this.actionsProvider.getActionById(this.actionId)
        .subscribe(res => {
          this.action = res.data[0]
          debugger
          this.setStateToAction()
          this.comments = this.action.comments
          this.formComment = this._formBuilder.group({
            action_id: [this.action.id || this.actionId, Validators.required],
            body: ['', Validators.required],
            created_at: [new Date(), Validators.required]
          })
          this.loaderAction.dismiss()
        }, error => {
          this.loaderAction.dismiss()
          debugger
        })
    } else {
      alert(this.actionId)
      this.loaderAction.dismiss()
    }
  }

  validateShowEditButton(assignedUserId): void {
    let userActual = JSON.parse(localStorage.getItem('currentUser')).id
    this.unlockEditAction = !(assignedUserId === userActual)
  }

  setStateToAction() {
    if (this.action.states_actions_id === 1)
      this.actionState = 'No iniciada'
    else if (this.action.states_actions_id === 2)
      this.actionState = 'En progreso'
    else if (this.action.states_actions_id === 3)
      this.actionState = 'Completada'
    else if (this.action.states_actions_id === 4)
      this.actionState = 'Expirada'
  }

  inicializeFormComment() {
    this.formComment = this._formBuilder.group({
      action_id: ['', Validators.required],
      body: ['', Validators.required],
      created_at: [new Date(), Validators.required]
    })
  }

  addComment() {
    if (this.formComment.valid) {
      let comment = this.formComment.value
      this.actionsProvider.createComment(comment)
        .subscribe(res => {
          if (localStorage.getItem('currentUser')) {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
            comment.user = {
              name: this.currentUser.name
            }
          }
          this.comments.push(comment)
        }, res => {
          debugger
        })
      this.formComment.controls.body.setValue('')
    }
  }

  editAction() {
    if (!_.isNumber(this.action.id)) {
      this.action.id = this.navParams.data
    }
    const modal = this.modalCtrl.create(CreateAction, { code: this.action.code, action: this.action })
    modal.present()
    modal.onDidDismiss(action => {
      if (action)
        this.action = action
    })
  }

  presentActionStateSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Cambiar el estado de la acción',
      subTitle: '',
      buttons: [
        {
          text: 'No comenzada',
          handler: () => {
            this.actionState = 'No comenzada'
            this.updateAction(this.action, 1)
          }
        },
        {
          text: 'En progreso',
          handler: () => {
            this.actionState = 'En progreso'
            this.updateAction(this.action, 2)
          }
        },
        {
          text: 'Completada',
          handler: () => {
            this.actionState = 'Completada'
            this.updateAction(this.action, 3)
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked')
          }
        }
      ]
    })

    actionSheet.present()
  }

  updateAction(action: Action, stateAction: number) {
    let newAction: any = {
      assigned_user_id: action.assigned_user_id || action.assigned.id,
      date_time_end: action.date_time_end,
      description: action.description,
      inspection_id: action.inspection_id,
      code: action.code || action.item_code,
      priority: action.priority,
      states_actions_id: stateAction
    }
    this.actionsProvider.updateAction(action.id || this.actionId, newAction)
      .subscribe(res => { }, error => {
        debugger
      })
  }

  removeAction() {
    let alert = this.alertCtrl.create({
      title: 'Confirmación',
      message: '¿Desea eliminar esta acción?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => { }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.actionsProvider.deleteAction(this.action.id)
              .subscribe(res => {
                this.navCtrl.pop({})
              }, error => {
                debugger
              })
          }
        }
      ]
    })
    alert.present()
  }

}
