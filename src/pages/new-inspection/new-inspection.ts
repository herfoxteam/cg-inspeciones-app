import { Component, OnInit } from '@angular/core'
import { NavController, NavParams, Nav, ModalController, LoadingController } from 'ionic-angular'
import { TemplateProvider } from '../../providers/template/template'
import { Template } from '../../components/template/template.model'
import { PreviewInspectionComponent } from '../../components/preview-inspection/preview-inspection'
import { Storage } from '@ionic/storage'
import { ConnectionProvider } from '../../providers/connection/connection';

@Component({
  selector: 'page-new-inspection',
  templateUrl: 'new-inspection.html',
})

export class NewInspectionPage {

  templates: Template[] = []
  searchText: string = ''
  notTemplates: boolean
  loader: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private templateProvider: TemplateProvider,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private connectionProvider: ConnectionProvider
  ) { }

  ionViewWillEnter() {
    this.getTemplates()
  }

  syncTemplates() {
    this.loader = this.loadingCtrl.create({
      content: 'cargando...'
    })
    this.loader.present()
    this.templateProvider.getTemplates()
      .subscribe(res => {
        this.storage.set('templates', JSON.stringify(res.data))
        this.templates = res.data
        this.validateTemplates()
        this.loader.dismiss()
      }, error => {
        this.loader.dismiss()
      })
  }

  getTemplates(event?) {
    if (this.connectionProvider.isConnected())
      this.syncTemplates()

    this.templateProvider.getTemplatesOffline()
      .then((val) => {
        this.templates = JSON.parse(val) || []
        this.validateTemplates()
        event ? event.complete() : null
      })
      .catch(error => {
        event ? event.complete() : null
        debugger
      })
  }

  validateTemplates() {
    if(this.templates)
      this.templates.length > 0 ? this.notTemplates = false : this.notTemplates = true
    else
      this.notTemplates = true
  }

  openPreviewInspection(template: Template) {
    template.from = 'template'
    let modal = this.modalCtrl.create(PreviewInspectionComponent, template, { cssClass: 'modal__preview' })
    modal.present()
  }

}
