import { BrowserModule } from '@angular/platform-browser'
import { ErrorHandler, NgModule } from '@angular/core'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular'
import { FilePath } from '@ionic-native/file-path'
import { MyApp } from './app.component'
import { HomePage } from '../pages/home/home'
import { ListPage } from '../pages/list/list'
import { ActionsPage } from '../pages/actions/actions'

import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'
import { InspectionsPage } from '../pages/inspections/inspections'
import { NewInspectionPage } from '../pages/new-inspection/new-inspection'
import { FormPage } from '../pages/form/form'
import { LoginPage } from '../pages/login/login'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { AuthProvider } from '../providers/auth/auth'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { ErrorsProvider } from '../providers/errors/errors'
import { PreviewInspectionComponent } from '../components/preview-inspection/preview-inspection'
import { TemplateProvider } from '../providers/template/template'
import { TemplateComponent } from '../components/template/template'
import { FormInputComponent } from '../components/form-input/form-input'
import { DynamicFieldDirective } from '../directives/dynamic-field/dynamic-field'
import { FormTextareaComponent } from '../components/form-textarea/form-textarea'
import { FormSwitchComponent } from '../components/form-switch/form-switch'
import { FormCheckboxComponent } from '../components/form-checkbox/form-checkbox'
import { FormGpsComponent } from '../components/form-gps/form-gps'
import { FormVideoComponent } from '../components/form-video/form-video'
import { FormImageComponent } from '../components/form-image/form-image'
import { FormSliderComponent } from '../components/form-slider/form-slider'
import { FormDataTimeComponent } from '../components/form-data-time/form-data-time'
import { Geolocation } from '@ionic-native/geolocation'
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture'
import { ActionsProvider } from '../providers/actions/actions'
import { ActionDetailPage } from '../pages/action-detail/action-detail'
import { ActionComponent } from '../components/action/action'
import { MultipleSelectionComponent } from '../components/multiple-selection/multiple-selection'
import { ScheduleProvider } from '../providers/schedule/schedule'
import { SchedulePage } from '../pages/schedule/schedule'
import { InspectionsProvider } from '../providers/inspections/inspections'
import { MediaProvider } from '../providers/media/media'
import { Camera } from '@ionic-native/camera'
import { Network } from '@ionic-native/network'
import { ConnectionProvider } from '../providers/connection/connection'
import { ConnectionComponent } from '../components/connection/connection'
import { FileTransfer } from '@ionic-native/file-transfer'
import { File } from '@ionic-native/file'
import { ImagePicker } from '@ionic-native/image-picker'
import { FormCategoryComponent } from '../components/form-category/form-category'
import { ExpandableComponent } from '../components/expandable/expandable'
import { AttachmentsComponent } from '../components/attachments/attachments'
import { ActionModalCommentsComponent } from '../components/action-modal-comments/action-modal-comments'
import { CreateAction } from '../components/action-modal-actions/create-action'
import { ModalListAcionsComponent } from '../components/modal-list-acions/modal-list-acions'
import { IonicStorageModule } from '@ionic/storage'
import { InterceptorProvider } from '../providers/interceptor/interceptor'
import { ActionModalImagesComponent } from '../components/action-modal-images/action-modal-images'
import { MomentModule } from 'angular2-moment'
import 'moment/locale/es'
import { IonicImageViewerModule } from 'ionic-img-viewer'
import { StreamingMedia } from '@ionic-native/streaming-media'
import { StatePipe } from '../pipes/state/state'
import { PriorityDirective } from '../directives/priority/priority'
import { InspectionAlertComponent } from '../components/inspection-alert/inspection-alert'
import { FilterListPipe } from '../pipes/filter-list/filter-list'
import { ModalTermsComponent } from '../components/modal-terms/modal-terms'
import { DatePipe } from '@angular/common'
import { FcmProvider } from '../providers/fcm/fcm'
import { Firebase } from '@ionic-native/firebase'
import { FileOpener } from '@ionic-native/file-opener'
import { ScreenOrientation } from '@ionic-native/screen-orientation'
import { VideoEditor } from '@ionic-native/video-editor';
import { SyncAllProvider } from '../providers/sync-all/sync-all';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ActionsPage,
    InspectionsPage,
    NewInspectionPage,
    FormPage,
    LoginPage,
    SchedulePage,
    PreviewInspectionComponent,
    MultipleSelectionComponent,
    FormInputComponent,
    FormTextareaComponent,
    FormSwitchComponent,
    FormCheckboxComponent,
    FormGpsComponent,
    FormVideoComponent,
    FormImageComponent,
    FormSliderComponent,
    FormDataTimeComponent,
    FormCategoryComponent,
    ExpandableComponent,
    AttachmentsComponent,
    DynamicFieldDirective,
    PriorityDirective,
    TemplateComponent,
    ActionDetailPage,
    ActionComponent,
    ConnectionComponent,
    ActionModalCommentsComponent,
    CreateAction,
    ModalTermsComponent,
    ActionModalImagesComponent,
    ModalListAcionsComponent,
    StatePipe,
    FilterListPipe,
    InspectionAlertComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { scrollPadding: false, scrollAssist: false, autoFocusAssist: false }),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MomentModule,
    IonicStorageModule.forRoot({
      name: 'checkonsite',
      driverOrder: ['sqlite', 'indexeddb', 'websql']
    }),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ActionsPage,
    InspectionsPage,
    NewInspectionPage,
    FormPage,
    LoginPage,
    PreviewInspectionComponent,
    MultipleSelectionComponent,
    FormInputComponent,
    FormTextareaComponent,
    FormSwitchComponent,
    FormCheckboxComponent,
    FormGpsComponent,
    FormVideoComponent,
    FormImageComponent,
    FormSliderComponent,
    FormDataTimeComponent,
    FormCategoryComponent,
    ExpandableComponent,
    AttachmentsComponent,
    ActionComponent,
    ActionDetailPage,
    SchedulePage,
    ConnectionComponent,
    ActionModalCommentsComponent,
    CreateAction,
    ModalTermsComponent,
    ActionModalImagesComponent,
    ModalListAcionsComponent,
    InspectionAlertComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthProvider,
    ErrorsProvider,
    TemplateProvider,
    Geolocation,
    MediaCapture,
    ActionsProvider,
    ScheduleProvider,
    InspectionsProvider,
    MediaProvider,
    Camera,
    Network,
    ConnectionProvider,
    FileTransfer,
    File,
    FilePath,
    FileOpener,
    ImagePicker,
    StreamingMedia,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorProvider, multi: true},
    FcmProvider,
    Firebase,
    ScreenOrientation,
    VideoEditor,
    SyncAllProvider,
    Facebook
  ]
})
export class AppModule {}
