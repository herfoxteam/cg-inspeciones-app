import { Component, ViewChild, OnInit } from '@angular/core'
import { Nav, Platform } from 'ionic-angular'
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'
import { HomePage } from '../pages/home/home'
import { ActionsPage } from '../pages/actions/actions'
import { InspectionsPage } from '../pages/inspections/inspections'
import { NewInspectionPage } from '../pages/new-inspection/new-inspection'
import { LoginPage } from '../pages/login/login'
import { ActionDetailPage } from '../pages/action-detail/action-detail'
import { SchedulePage } from '../pages/schedule/schedule'
import { ConnectionProvider } from '../providers/connection/connection'
import { Storage } from '@ionic/storage'
import { _throw } from 'rxjs/observable/throw'
import { FcmProvider } from '../providers/fcm/fcm'
import { ToastController } from 'ionic-angular'
import { tap } from 'rxjs/operators'
import { ScreenOrientation } from '@ionic-native/screen-orientation'
import { File } from '@ionic-native/file'
import { SyncAllProvider } from '../providers/sync-all/sync-all';
import { AuthProvider } from '../providers/auth/auth';
import { JsonPipe } from '@angular/common';

@Component({
  templateUrl: 'app.html'
})

export class MyApp implements OnInit {

  rootPage: any
  @ViewChild(Nav) nav: Nav
  currentUser: any

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private connectionProvider: ConnectionProvider,
    private storage: Storage,
    private fcmProvider: FcmProvider,
    private toastCtrl: ToastController,
    private screenOrientation: ScreenOrientation,
    private file: File,
    private syncAllProvider: SyncAllProvider,
    private authProvider: AuthProvider,
  ) {
    this.initializeApp()
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
    }
  }

  initializeApp() {
    // Sync all data for oofline
    this.platform.resume.subscribe(res => {
      if(this.authProvider.isAuthenticated)
        this.syncAllProvider.syncAllData()
    })
    if(this.authProvider.isAuthenticated)
      this.syncAllProvider.syncAllData()

    // Validation token
    this.storage.get('token').then(token => {
      if (token)
        this.rootPage = HomePage
      else
        this.rootPage = LoginPage
    }, error => {
      this.rootPage = LoginPage
    })

    this.platform.ready().then(() => {
      if (!this.platform.is('core')) {

        this.createFoldersForApp()

        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT)

        this.fcmProvider.listenToNotifications().pipe(
          tap(msg => {
            if (msg['google.message_id']) {
              if (msg.type === 'action') {
                this.rootPage = ActionsPage
                this.nav.push(ActionDetailPage, msg.id)
              } else if (msg.type === 'schedule')
                this.nav.setRoot(SchedulePage)
            } else {
              const toast = this.toastCtrl.create({
                message: `${msg.title} - ${msg.body}`,
                duration: 8000,
                position: 'top',
                closeButtonText: '',
                showCloseButton: false
              });
              toast.present();
            }
          })
        ).subscribe()
      }

      this.connectionProvider.initializeNetworkEvents()
      this.statusBar.styleDefault()
      this.splashScreen.hide()
    })
  }

  createFoldersForApp() {
    this.file.createDir(this.file.externalDataDirectory, 'images', true).then(res => {
      console.log('EXITODO CREACION FOLDER IMAGES')
    })
    .catch(error => {
      console.log('ERROR CREACION FOLDER IMAGES' + JSON.stringify(error));

    })
    this.file.createDir(this.file.externalDataDirectory, 'videos', true)
  }

  openPage(page) {
    if (page === 'schedule')
      this.nav.setRoot(SchedulePage)
    else if (page === 'actions')
      this.nav.setRoot(ActionsPage)
    else if (page === 'inspections')
      this.nav.setRoot(InspectionsPage)
    else if (page === 'new')
      this.nav.setRoot(NewInspectionPage)
    else if (page === 'dashboard')
      this.nav.setRoot(HomePage)
  }

  logout() {
    this.storage.remove('token')
    this.nav.setRoot(LoginPage)
    localStorage.clear()
    this.storage.clear()
    if (this.connectionProvider.isConnected())
      this.authProvider.logout()
  }
}
