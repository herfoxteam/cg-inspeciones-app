import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { ConnectionProvider } from '../connection/connection'
import { ActionsProvider } from '../actions/actions'
import { ScheduleProvider } from '../schedule/schedule'
import { TemplateProvider } from '../template/template'
import { Storage } from '@ionic/storage'
import { InspectionsProvider } from '../inspections/inspections'
import { Inspection } from '../../pages/inspections/inspection.model'
import { Schedule } from '../../pages/schedule/schedule.model'
import { _ } from 'underscore'
import { Action } from '../../pages/actions/action.model';
import { Observable } from 'rxjs/Observable';
import { Template } from '../../components/template/template.model';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class SyncAllProvider {

  constructor(
    public http: HttpClient,
    private connectionProvider: ConnectionProvider,
    private actionsProvider: ActionsProvider,
    private scheduleProvider: ScheduleProvider,
    private templateProvider: TemplateProvider,
    private storage: Storage,
    private inspectionsProvider: InspectionsProvider,
    private authProvider: AuthProvider
  ) { }

  syncAllData(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        if (this.authProvider.isAuthenticated) {

          Promise.all([
            this.syncTemplates(),
            this.syncActions(),
            this.syncSchedule(),
            this.syncScheduleToday(),
            this.syncActionsByDays(),
            this.syncUsers()
          ])
            .then((result) => {
              debugger
              resolve(result)
            }).catch((error) => {
              debugger
              reject(error)
            })
        }
        else
          reject('No fue posible sincronizar su información')
      }
      else
        reject('No fue posible sincronizar su información')
    })
  }

  syncInspections() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.inspectionsProvider.getInspectionsSync()
          .subscribe(res => {
            let inspections: Array<Inspection> = res.data
            resolve(this.inspectionsProvider.setInspectionsOffline(inspections))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar sus inspecciones')
    })
  }

  syncTemplates() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.templateProvider.getTemplates()
          .subscribe(res => {
            let templates: Array<Template> = res.data
            resolve(this.storage.set('templates', JSON.stringify(templates)))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar los templates')
    })
  }

  syncActions() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.actionsProvider.getActions()
          .subscribe(res => {
            let actions: Array<Action> = res.data
            resolve(this.actionsProvider.saveActionsOffline(actions))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar las acciones')
    })
  }

  syncSchedule() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.scheduleProvider.getSchedule()
          .subscribe(res => {
            let schedule: Array<Schedule> = res.data
            resolve(this.scheduleProvider.getScheduleOfflineAndSync(schedule))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar el cronograma')
    })
  }

  syncScheduleToday() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.scheduleProvider.getScheduleToday()
          .subscribe(res => {
            let scheduledToday: Schedule[] = res.data
            resolve(this.storage.set('scheduledToday', JSON.stringify(scheduledToday)))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar el cronograma para hoy')
    })
  }

  syncActionsByDays() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.actionsProvider.getActionsByDays(3)
          .subscribe(res => {
            let actionsByDays: Action[] = res.data
            resolve(this.storage.set('actionsByDays', JSON.stringify(actionsByDays)))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar las acciones')
    })
  }

  syncUsers() {
    return new Promise((resolve, reject) => {
      if (this.connectionProvider.isConnected()) {
        this.actionsProvider.getUsers()
          .subscribe(res => {
            let users: Array<any> = res.data
            resolve(this.storage.set('users', JSON.stringify(users)))
          }, error => { error })
      }
      else
        reject('No fue posible sincronizar los usuarios responsables')
    })
  }

}
