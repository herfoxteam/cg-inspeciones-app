import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { Observable } from 'rxjs/Observable'
import { Storage } from '@ionic/storage'
import { _ } from 'underscore'

@Injectable()
export class TemplateProvider {

  readonly TEMPLATES = `${environment.urlApi}/templates`

  constructor(public http: HttpClient, private storage: Storage) { }

  getTemplates(): Observable<any> {
    return this.http.get<any>(`${this.TEMPLATES}`)
  }

  getTemplatesOffline(): Promise<any> {
    return this.storage.get('templates')
  }

  getTemplateById(id: number): Observable<any> {
    return this.http.get<any>(`${this.TEMPLATES}/${id}`)
  }

  getTemplateByIdOffline(idTemplate: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getTemplatesOffline()
        .then(val => {
          let templates = JSON.parse(val) || []
          resolve(_.findWhere(templates, { id: idTemplate }))
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }
}
