import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { Observable } from 'rxjs/Observable'
import { Storage } from '@ionic/storage'
import { _ } from 'underscore'
import { Schedule } from '../../pages/schedule/schedule.model';

@Injectable()
export class ScheduleProvider {

  readonly SCHEDULE = `${environment.urlApi}/schedules`

  constructor(public http: HttpClient, private storage: Storage) { }

  getSchedule(): Observable<any> {
    return this.http.get<any>(this.SCHEDULE)
  }

  getScheduleOffline(): Promise<any> {
    return this.storage.get('schedule')
  }

  getScheduleByIdOffline(idSchedule: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getScheduleOffline()
        .then(val => {
          let schedule = JSON.parse(val) || []
          resolve(_.findWhere(schedule, { id: idSchedule }))
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  updateScheduleOffline(schedule: Schedule | any, idInspection: number): Promise<any> {
    return new Promise((resolve, reject) => {

      this.getScheduleOffline()
        .then((val: string) => {
          let listSchedule: Schedule[] = JSON.parse(val) || []
          for (let i = 0; i < listSchedule.length; i++) {
            if (listSchedule[i].id === schedule.id)
              listSchedule[i].inspection_id = idInspection
          }
          resolve(this.saveScheduleOffline(listSchedule))
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })

    })
  }

  saveScheduleOffline(schedule: Schedule[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.set('schedule', JSON.stringify(schedule))
        .then(val => {
          resolve(schedule)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  getScheduleToday(): Observable<any> {
    return this.http.get<any>(`${this.SCHEDULE}?next_days=0`)
  }

  getScheduleTodayOffline(): Promise<any> {
    return this.storage.get('scheduledToday')
  }

  getScheduleOfflineAndSync(listScheduleServer: Schedule[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getScheduleOffline()
        .then((res: Schedule[]) => { return res })
        .then((listScheduleOffline: Schedule[]) => {
          resolve(this.syncSchedule(listScheduleOffline, listScheduleServer))
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  syncSchedule(listScheduleOffline: Schedule[], listScheduleServer: Schedule[]): Promise<any> {
    return new Promise((resolve, reject) => {
      if (_.isArray(listScheduleOffline)) {
        listScheduleOffline.forEach((itemOffline: Schedule) => {
          listScheduleServer.forEach((itemServer: Schedule) => {
            if (itemOffline.id === itemServer.id) {
              itemOffline.state = itemServer.state
              itemOffline.end = itemServer.end
              itemServer.inspection_id ? itemOffline.inspection_id = itemServer.inspection_id : null
            }
          })
        })
        resolve(this.saveScheduleOffline(listScheduleOffline))
      }
      else
        resolve(this.saveScheduleOffline(listScheduleServer))
    })
  }
}
