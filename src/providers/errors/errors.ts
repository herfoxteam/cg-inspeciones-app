import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable()
export class ErrorsProvider {

  constructor(public http: HttpClient) { }

  handlerErrors(error) {
    let result: any = {
      title: '',
      subTitle: '',
      buttons: ['OK']
    }
    if (error.error) {
      result.title = 'Ha ocurrido un error'
      result.subTitle = error.error.message
    }
    return result
  }

}
