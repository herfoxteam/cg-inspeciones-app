import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { CreateInspectionFromSchedule, CreateInspectionFromTemplate, Inspection } from '../../pages/inspections/inspection.model'
import { Storage } from '@ionic/storage'
import { _ } from 'underscore'

@Injectable()
export class InspectionsProvider {

  readonly INSPECTIONS = `${environment.urlApi}/inspections`
  readonly SYNC_INSPECTIONS = `${environment.urlApi}/sync-all-inspections`
  readonly SAVE_INSPECTION = `${environment.urlApi}/save-inspection`

  constructor(public http: HttpClient, private storage: Storage) { }

  getInspections(): Observable<any> {
    return this.http.get<any>(`${this.INSPECTIONS}`)
  }

  getInspectionsSync(): Observable<any> {
    return this.http.get<any>(`${this.SYNC_INSPECTIONS}`)
  }

  getInspectionsOffline(): Promise<any> {
    return this.storage.get('inspections')
  }

  setInspectionsOffline(inspections: Array<Inspection>): Promise<any> {
    return this.storage.set('inspections', JSON.stringify(inspections))
  }

  getInspectionById(id: number): Observable<any> {
    return this.http.get<any>(`${this.INSPECTIONS}/${id}`)
  }

  createInspection(data: CreateInspectionFromSchedule | CreateInspectionFromTemplate): Observable<any> {
    return this.http.post<any>(`${this.INSPECTIONS}`, data)
  }

  createInspectionOffline(data: Inspection): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getInspectionsOffline()
        .then(val => {
          let inspections = JSON.parse(val) || []
          inspections.push(data)
          this.storage.set('inspections', JSON.stringify(inspections))
            .then(val => {
              resolve(data)
            })
            .catch(error => {
              debugger
              reject(error)
            })
        })
        .catch(error => {
          debugger
          reject(error)
          alert('error')
        })
    })
  }

  getInspectionByIdOffline(idInspection: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getInspectionsOffline()
        .then(val => {
          let inspections = JSON.parse(val) || []
          resolve(_.findWhere(inspections, { id: idInspection }))
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  updateInspectionOffline(inspection: Inspection, newId?: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get('inspections')
        .then(val => {
          let inspections = JSON.parse(val) || []
          for (let i = 0; i < inspections.length; i++) {
            if (inspections[i].id === inspection.id) {
              inspections[i] = inspection
              newId ? inspections[i].id = newId : null
            }
          }
          this.storage.set('inspections', JSON.stringify(inspections))
            .then(() => {
              newId ? inspection.id = newId : null
              resolve(inspection)
            })
            .catch(error => {
              console.log(error)
              reject(error)
            })
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })

    })
  }

  saveInspection(idInspection: number, responsesInspection: any, type: 'terminate' | 'save'): Observable<any> {
    const { data, actions, observations, images } = responsesInspection
    return this.http.post<any>(`${this.SAVE_INSPECTION}`, {
      inspections_id: idInspection,
      data,
      observations,
      actions,
      images,
      type
    })
  }

  saveInspectionOffline(idInspection: number, responsesInspection: any, type: 'terminate' | 'save'): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getInspectionByIdOffline(idInspection)
        .then(res => {
          let inspection = res
          inspection.storage = JSON.stringify(responsesInspection)
          this.updateInspectionOffline(inspection)
            .then((res: Inspection) => {
              resolve(res)
            })
            .catch(error => {
              console.log(error)
              reject(error)
            })
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  getTitleInspection(inspection: Inspection): string {
    let viewTitle
    if (inspection.scheduled)
      inspection.scheduled.title ? viewTitle = inspection.scheduled.title : null
    else if (inspection.template)
      inspection.template.title ? viewTitle = inspection.template.title : null
    return viewTitle
  }


  /*
  * PUSH AND SYNC TO SERVER
  */

  pushOfflineInspectionsToServer() {
    return new Promise((resolve, reject) => {
      this.getInspectionsOffline()
        .then(val => {
          let inspections: Array<Inspection> = JSON.parse(val) || []
          let promises = []
          inspections.forEach((inspection: Inspection) => {
            if (inspection.sync === false && inspection.stepsSyncOffline) {
              if (!inspection.stepsSyncOffline.one) {
                promises.push(
                  this.pushOfflineInspection(inspection)
                    .then(res => res)
                    .catch(error => {
                      console.log(error)
                      reject(error)
                    })
                )
              }
            }
          })
          Promise.all(promises).then(res => {
            debugger
            resolve(true)
          }).catch(error => {
            console.log(error)
            debugger
            reject(error)
          })
        })
        .catch(error => { reject(error) })
    })
  }

  private pushOfflineInspection(inspection: Inspection): Promise<any> {
    return new Promise((resolve, reject) => {

      this.stepCreateInspection(inspection)
        .then((inspection: Inspection) => {
          return this.stepSaveInspection(inspection)
        })
        .then((inspection: Inspection) => {
          resolve(inspection)
        })
        .catch(error => {
          debugger
          reject(error)
        })
    })
  }

  stepCreateInspection(inspection: Inspection): Promise<any> {
    return new Promise((resolve, reject) => {
      let data: CreateInspectionFromTemplate | CreateInspectionFromSchedule
      if (inspection.from === 'template')
        data = { date_time_start: inspection.start_at, template_id: inspection.template_id }
      else if (inspection.from === 'schedule')
        data = { date_time_start: inspection.start_at, event_id: inspection.scheduled.id }

      this.createInspection(data)
        .subscribe(res => {
          inspection.stepsSyncOffline.one = true
          let newId = res.data.id
          resolve(this.updateInspectionOffline(inspection, newId))
        }, error => { reject(error) })
    })
  }

  stepSaveInspection(inspection: Inspection): Promise<any> {
    return new Promise((resolve, reject) => {
      if (inspection.storage) {
        let responsesInspection = JSON.parse(inspection.storage)
        this.saveInspection(inspection.id, responsesInspection, 'save')
          .subscribe(res => {
            inspection.stepsSyncOffline.two = true
            resolve(this.updateInspectionOffline(inspection))
          }, error => {
            console.log(error)
            reject(error)
          })
      }
      else
        reject('no hay storage para guardar')
    })
  }

}
