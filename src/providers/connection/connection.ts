import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Network } from '@ionic-native/network'
import { Events } from 'ionic-angular'
import { InspectionsProvider } from '../inspections/inspections';

export enum ConnectionStatusEnum {
  Online,
  Offline
}

@Injectable()
export class ConnectionProvider {

  previousStatus: any

  constructor(
    public http: HttpClient,
    private network: Network,
    public eventCtrl: Events,
    private inspectionsProvider: InspectionsProvider
  ) {
    this.previousStatus = ConnectionStatusEnum.Online
  }

  isConnected(): boolean {
    let conntype = this.network.type
    return conntype && conntype !== 'unknown' && conntype !== 'none'
  }

  initializeNetworkEvents() {
    this.network.onDisconnect().subscribe(() => {
      if (this.previousStatus === ConnectionStatusEnum.Online) {
        this.eventCtrl.publish('network:offline')
      }
      this.previousStatus = ConnectionStatusEnum.Offline
      console.log('network was disconnected :-(')
    })

    this.network.onConnect().subscribe(() => {
      if (this.previousStatus === ConnectionStatusEnum.Offline) {
        this.eventCtrl.publish('network:online')
      }
      if(this.isConnected())
        this.inspectionsProvider.pushOfflineInspectionsToServer()
      this.previousStatus = ConnectionStatusEnum.Online
      console.log('network connected!')
    })
  }
}
