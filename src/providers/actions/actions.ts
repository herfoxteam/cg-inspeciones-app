import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import { Action } from '../../pages/actions/action.model';
import { Storage } from '@ionic/storage'
import { InspectionsProvider } from '../inspections/inspections';
import { _ } from 'underscore'
import { Inspection } from '../../pages/inspections/inspection.model';

@Injectable()
export class ActionsProvider {

  readonly ACTIONS = `${environment.urlApi}/actions`
  readonly MY_ACTIONS = `${environment.urlApi}/my_actions`
  readonly OBSERVATIONS = `${environment.urlApi}/observations`
  readonly COMMENTS = `${environment.urlApi}/comments`
  readonly IMAGES = `${environment.urlApi}/images`
  readonly USER_SEARCH = `${environment.urlApi}/search-user`
  readonly USERS = `${environment.urlApi}/users`

  constructor(
    public http: HttpClient,
    private storage: Storage,
    private inspectionsProvider: InspectionsProvider
  ) { }

  getActions(): Observable<any> {
    return this.http.get<any>(this.ACTIONS)
  }

  getActionById(id: number): Observable<any> {
    return this.http.get<any>(`${this.ACTIONS}/${id}`)
  }

  getActionsByDays(numberDays: number): Observable<any> {
    return this.http.get<any>(`${this.ACTIONS}?next_days=${numberDays}`)
  }

  getActionsOffline(): Promise<any> {
    return this.storage.get('allActions')
  }

  getActionsFromInspections(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.inspectionsProvider.getInspectionsOffline()
        .then(val => {
          let inspections = JSON.parse(val) || []
          let actions = []
          inspections.forEach((inspection: Inspection) => {
            inspection.actions.forEach((action: Action) => {
              actions.push(action)
            })
          })
          debugger
          // resolve(this.setActionsOffline(actions))
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  saveActionsOffline(actions: Action[]): Promise<any> {
    return this.storage.set('allActions', JSON.stringify(actions))
  }

  getMyActions(): Observable<any> {
    return this.http.get<any>(this.MY_ACTIONS)
  }

  getMyActionsOffline(): Promise<any> {
    return this.storage.get('myActions')
  }

  getMyActionByIdOffline(idAction: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getMyActionsOffline()
        .then(val => {
          let actions = JSON.parse(val) || []
          resolve(_.findWhere(actions, { id: idAction }))
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  getResults(keyword: string): Observable<any> {
    return this.http.get<any>(`${this.USER_SEARCH}?q=${keyword}`)
  }

  getUsers(): Observable<any> {
    return this.http.get<any>(`${this.USERS}`)
  }

  getUsersOffline(): Promise<any> {
    return this.storage.get('users')
  }

  createAction(action: any): Observable<any> {
    action.code = new Date().getTime()
    return this.http.post<any>(this.ACTIONS, action)
  }

  createActionOffline(action: Action): Promise<any> {
    return new Promise((resolve, reject) => {
      this.inspectionsProvider.getInspectionByIdOffline(action.inspection_id)
        .then(val => {
          let inspection = val
          inspection.actions.push(action)
          this.inspectionsProvider.updateInspectionOffline(inspection)
            .then(res => {
              resolve(inspection.actions)
            })
            .catch(error => {
              reject(error)
            })
        }, error => {
          console.log(error)
          reject(error)
        })
    })
  }

  removeMyActionsOffline(): Promise<any> {
    return this.storage.remove('myActions')
  }

  // this.getMyActionsOffline()
  // .then(val => {
  //   let actions = JSON.parse(val) || []
  //   actions.push(action)
  //   this.storage.set('myActions', JSON.stringify(actions))
  //     .then(val => {
  //       resolve(action)
  //     })
  //     .catch(error => {
  //       console.log(error)
  //       reject(error)
  //     })
  // })
  // .catch(error => {
  //   console.log(error)
  //   reject(error)
  // })

  updateAction(id: number, action: Action): Observable<any> {
    return this.http.put<any>(`${this.ACTIONS}/${id}`, action)
  }

  updateActionOffline(idAction: number, newAction: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getMyActionByIdOffline(idAction)
        .then(res => {
          let action: Action = res
          let {
            assigned_user_id,
            date_time_end,
            description,
            inspection_id,
            priority
          } = newAction
          action.assigned_user_id = assigned_user_id
          action.date_time_end = date_time_end
          action.description = description
          action.priority = priority
          resolve(this.updateActionsOffline(action))
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  updateActionsOffline(action: Action): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getMyActionsOffline()
        .then(val => {
          let actions = JSON.parse(val) || []
          for (let i = 0; i < actions.length; i++) {
            if (actions[i].id === action.id)
              actions[i] = action
          }
          resolve(this.storage.set('myActions', JSON.stringify(actions)))
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })

    })
  }

  deleteAction(id: number): Observable<any> {
    return this.http.delete<any>(`${this.ACTIONS}/${id}`)
  }

  createObservation(observation: any): Observable<any> {
    observation.code = new Date().getTime()
    return this.http.post<any>(this.OBSERVATIONS, observation)
  }

  createObservationOffline(observation: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.inspectionsProvider.getInspectionByIdOffline(observation.inspection_id)
        .then(val => {
          let inspection = val
          inspection.observations.push(observation)
          resolve(this.inspectionsProvider.updateInspectionOffline(inspection))
        }, error => {
          console.log(error)
          resolve(error)
        })

    })
  }

  createComment(comment: any): Observable<any> {
    return this.http.post<any>(this.COMMENTS, comment)
  }

  createImage(image: any): Observable<any> {
    image.code = new Date().getTime()
    return this.http.post<any>(this.IMAGES, image)
  }

  createImageOffline(image: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.inspectionsProvider.getInspectionByIdOffline(image.inspection_id)
        .then(val => {
          let inspection = val
          inspection.images.push(image)
          resolve(this.inspectionsProvider.updateInspectionOffline(inspection))
        }, error => {
          console.log(error)
          reject(error)
        })

    })
  }

  deleteImageOffline(image: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.inspectionsProvider.getInspectionByIdOffline(image.inspection_id)
        .then(val => {
          let inspection = val
          let index = _.indexOf(inspection.images, image)
          if (index) {
            inspection.images.splice(index, 1)
          }
          resolve(this.inspectionsProvider.updateInspectionOffline(inspection))
        }, error => {
          console.log(error)
          reject(error)
        })

    })
  }

  getAllActionsAndSync(listActionsServer: Action[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getActionsOffline()
        .then((res: Action[]) => { return res })
        .then((listActionsOffline: Action[]) => {
          return this.syncAllActions(listActionsOffline, listActionsServer)
        })
    })
  }


  /*
  * PUSH AND SYNC WITH SERVER
  */

  syncAllActions(listActionsOffline: Action[], listActionsServer: Action[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getActionsFromInspections()
        .then(val => {
          debugger
        })
        .catch(error => {
          debugger
        })


      if (_.isArray(listActionsOffline)) {
        listActionsOffline.forEach((actionOffline: Action) => {
          listActionsServer.forEach((actionServer: Action) => {
            if (actionOffline.id === actionServer.id) {
              actionOffline.date_time_end = actionServer.date_time_end
              actionOffline.closed = actionServer.closed
              actionOffline.assigned_user_id = actionServer.assigned_user_id
              actionOffline.item_id = actionServer.item_id
              actionOffline.updated_at = actionServer.updated_at
              actionOffline.updated_at = actionServer.updated_at
              actionOffline.state = actionServer.state
              actionOffline.state = actionServer.state
              actionOffline.states_actions_id = actionServer.states_actions_id
              actionOffline.assigned = actionServer.assigned
            }
          })
        })

        resolve(this.saveActionsOffline(listActionsOffline))
      }
      else
        resolve(this.saveActionsOffline(listActionsServer))
    })
  }

}
