import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { Observable } from 'rxjs'
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer'
import { File } from '@ionic-native/file'
import { Storage } from '@ionic/storage'

@Injectable()
export class MediaProvider {

  readonly VIDEO = `${environment.urlApi}/processing-video`
  readonly IMAGE = `${environment.urlApi}/processing-image`

  constructor(
    public http: HttpClient,
    private transfer: FileTransfer,
    private file: File,
    private storage: Storage
  ) { }

  processingVideo(urlVideo: any) {

    let promise = this.storage.get('token')

    return Observable.fromPromise(promise)
      .mergeMap(token => {
        const fileTransfer: FileTransferObject = this.transfer.create();
        let options: FileUploadOptions = {
          fileKey: 'video',
          fileName: 'name.mp4',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT'
          }
        }
        return fileTransfer.upload(urlVideo, this.VIDEO, options)
      })
  }

  processingImage(urlImage: any) {

    let promise = this.storage.get('token')

    return Observable.fromPromise(promise)
      .mergeMap(token => {
        const fileTransfer: FileTransferObject = this.transfer.create();
        let options: FileUploadOptions = {
          fileKey: 'image',
          fileName: 'name.jpg',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT'
          }
        }
        return fileTransfer.upload(urlImage, this.IMAGE, options)
      })
  }
}

