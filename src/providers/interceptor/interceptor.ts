import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Storage } from '@ionic/storage'
import { AlertController } from 'ionic-angular'
import { Observable } from 'rxjs'
import { _throw } from 'rxjs/observable/throw'
import { catchError } from 'rxjs/operators'

@Injectable()
export class InterceptorProvider implements HttpInterceptor {

  constructor(
    public http: HttpClient,
    private storage: Storage,
    private alertCtrl: AlertController
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let promise = this.storage.get('token')

    return Observable.fromPromise(promise)
      .mergeMap(token => {

        request = request.clone({
          setHeaders: {
            'Accept': `application/json`,
            'Content-Type': `application/json`,
            Authorization: `Bearer ${token}`
          },
        })

        return next.handle(request).pipe(
          catchError(error => {
            if (error.status) {
              if (error.status !== 0) {
                let msg = error.error.message || error.error || 'Ha ocurrido un error'
                let alert = this.alertCtrl.create({
                  subTitle: msg,
                  buttons: ['OK']
                })
                alert.present()
              }
            }
            return _throw(error)
          })
        )
      })
  }
}
