import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { Observable } from 'rxjs/Observable'

@Injectable()
export class AuthProvider {

  readonly AUTH = `${environment.urlBase}/api/auth`
  readonly REGISTER = `${environment.urlBase}/api/v1/register`
  readonly DEVICE_TOKEN = `${environment.urlBase}/api/v1/device_tokens`

  constructor(public http: HttpClient) { }

  register(data): Observable<any> {
    return this.http.post<any>(`${this.REGISTER}/userauth`, data)
  }

  login(data): Observable<any> {
    return this.http.post<any>(`${this.AUTH}/login`, data)
  }

  verifyAuth(): Observable<any> {
    return this.http.get<any>(`${this.AUTH}/verify`)
  }

  logout(): Observable<any> {
    return this.http.get<any>(`${this.AUTH}/logout`)
  }

  deviceToken(data): Observable<any> {
    return this.http.post<any>(`${this.DEVICE_TOKEN}`, data)
  }

  isAuthenticated(): boolean {
    let result = JSON.parse(localStorage.getItem('currentUser'))
    if (result !== '' && result !== null && result !== undefined && result !== {})
      return true
    else
      return false
  }

}
