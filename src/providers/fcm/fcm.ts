import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Firebase } from '@ionic-native/firebase'
import { Platform } from 'ionic-angular'
import { AuthProvider } from '../auth/auth'

@Injectable()
export class FcmProvider {

  constructor(
    public http: HttpClient,
    private firebase: Firebase,
    public platform: Platform,
    private authProvider: AuthProvider
  ) { }

  async getToken() {

    let token
    let platform

    if (this.platform.is('android')) {
      token = await this.firebase.getToken()
      platform = 'android'
    }

    if (this.platform.is('ios')) {
      token = await this.firebase.getToken()
      await this.firebase.grantPermission()
      platform = 'ios'
    }
    this.authProvider.deviceToken({ token, platform })
  }

  listenToNotifications() {
    return this.firebase.onNotificationOpen()
  }

}
